"""
- | is a vertical pipe connecting north and south.
- - is a horizontal pipe connecting east and west.
- L is a 90-degree bend connecting north and east.
- J is a 90-degree bend connecting north and west.
- 7 is a 90-degree bend connecting south and west.
- F is a 90-degree bend connecting south and east.
- . is ground; there is no pipe in this tile.
- S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.
"""


import os
import sys
from tabulate import tabulate
sys.setrecursionlimit(50000)



m = [[]]


def walk(coords: tuple, dir: str, distance: int):
    if distance == sys.getrecursionlimit()-1:
        print("MAXRECURSION")
        return
    # print(coords)
    if m[coords[0]][coords[1]] == distance or m[coords[0]][coords[1]] == distance +1:
        print("FINALDIST", distance )
        return
    curloc = inp[coords[0]][coords[1]]
    if curloc == ".":
        return
    # print("walking: ", coords, distance, dir, curloc)
    m[coords[0]][coords[1]] = distance
    if curloc == "S" and distance == 0:
        walk((coords[0]+1, coords[1]), "south", distance +1)
        walk((coords[0], coords[1]+1), "east", distance +1)
        walk((coords[0]-1, coords[1]), "north", distance +1)
        walk((coords[0], coords[1]-1), "west", distance +1)
    if curloc == "-":
        if dir == "east":
            walk((coords[0], coords[1]+1), "east", distance + 1)
        else:
            walk((coords[0], coords[1]-1), "west", distance +1)
    if curloc == "|":
        if dir == "north":
            walk((coords[0]-1, coords[1]), "north", distance +1 )
        else:
            walk((coords[0]+1, coords[1]), "south", distance +1)
    if curloc == "L":
        if dir == "south":
            walk((coords[0], coords[1]+1),"east", distance + 1)
        elif dir == "west":
            walk((coords[0]-1, coords[1]),"north", distance + 1)
    elif curloc == "J":
        if dir == "south":
            walk((coords[0], coords[1]-1),"west", distance + 1)
        elif dir == "east":
            walk((coords[0]-1, coords[1]),"north", distance + 1)
    elif curloc == "7":
        if dir == "north":
            walk((coords[0], coords[1]-1),"west", distance + 1)
        elif dir == "east":
            walk((coords[0]+1, coords[1]),"south", distance + 1)
    elif curloc == "F":
        if dir == "north":
            walk((coords[0], coords[1]+1),"east", distance + 1)
        elif dir == "west":
            walk((coords[0]+1, coords[1]),"south", distance + 1)
    

checked_cells = []


print(checked_cells)
def check_neighbours(coords: tuple, count):
    # print("COORDS", coords[0], coords[1], count, len(m), len(m[0]))
    # if count == 10:
    #     return
    for x in range(-1, 2):
        for y in range(-1, 2):
            if coords[0]+x == len(m) or coords[1]+y == len(m[0]) or coords[0]+x <= 0 or coords[1]+y <= 0:
                checked_cells[coords[0]][coords[1]] = "O"
                return
            else:
                checked_cells[coords[0]][coords[1]] = "K"
    # print(tabulate(m))
    # print(tabulate(checked_cells))

# def check_again(coords: tuple, count):
#     x,y = coords
#     print("COORDS", coords[0], coords[1], count, len(checked_cells), len(checked_cells[0]))
#     if m[x][y] != -1:
#         return
#     for dx in range(-1, 2):
#         for dy in range(-1, 2):
#             if dx == 0 and dy == 0:
#                 continue
#             nx, ny = x+dx, y+dy
#             if 0 <= nx < len(m) and 0 <= ny < len(checked_cells[0]) and checked_cells[nx][ny] == 'K':
#                 checked_cells[x][y] = 'K'
#                 return
#     checked_cells[x][y] = "O"


# def check_again(coords:tuple, count):
#     print("COORDS", coords[0], coords[1], count ,checked_cells[coords[0]][coords[1]])
#     for x in range(-1, 1):
#         for y in range(-1, 1):
#             if coords[0]+x < 0 or coords[1]+y < 0 or (x == 0 and y ==0):
#                 continue
#             if checked_cells[coords[0]+x][coords[1]+y] == "K" and checked_cells[coords[0]][coords[1]] == "O":
#                 checked_cells[coords[0]][coords[1]] = "O"
#                 check_again((coords[0]+x, coords[1]+y), count +1)
#                 print("hcanged", coords[0], coords[1])
#             return count

def check_again():
    for x in range(len(inp)):
        for y in range(len(inp[0])):
                for i in range(-1,2):
                    for j in range(-1,2):
                        _x, _y = len(inp)-1-(x+i), len(inp[0])-1 -(y+j)
                        # if (i == -1 and j == -1) or (i == 1 and j == 1) or (i == 1 and j == -1) or (i == -1 and j == 1):
                        #     continue

                        if 0 <= x+i < len(inp) and 0 <= y+j < len(inp[0]):
                            if (m[x+i][y+j] == "|" or inp[x+i][y+j] == "-") and inp[x][y] == "K":
                                checked_cells[x][y] = "O"

                        if checked_cells[x][y] == "O":
                            if 0 <= x+i < len(inp) and 0 <= y+j < len(inp[0]):
                                # print("CHCEKCING", x, y)
                                # print("CHCEKCING2", len(inp)-(x+(-1*i)), len(inp[0])-(y-(-1*j)))
                                if checked_cells[x+i][y+j] == "K":
                                    # print("CHANING", x+i, y+j)
                                    checked_cells[x+i][y+j] = "O"
                                    # print(tabulate(checked_cells))
                        if checked_cells[len(inp)-1-(x)][len(inp[0])-1-y] == "O":
                            if 0 <= _x < len(inp) and 0 <= _y < len(inp[0]):
                                if checked_cells[_x][_y] == "K":
                                    print("CHANING2", _x, _y)
                                    checked_cells[_x][_y] = "O"
                                    # print(tabulate(checked_cells))




                
def fill():
    for x in range(len(inp)):
        for y in range(len(inp[0])):
            if m[x][y] == -1:
                if (x,y) not in checked_cells:
                    # amount = count_emtpy((x, y), 1, (x,y))
                    amount = check_neighbours((x,y), 1)
                    # amount = check_neighbours((len(inp)-x, len(inp[0])-1-y), 1)
                    # check_again((x,y), 1)
                    # check_again((len(inp)-1-x, len(inp[0])-1-y), 1)
    # print(tabulate(m))
    # print(tabulate(checked_cells))
    check_again()
    # print(tabulate(checked_cells))
    print(tabulate(checked_cells))
    print(sum(x.count('K') for x in checked_cells))
    check_again()
    print(tabulate(checked_cells))
    print(sum(x.count('K') for x in checked_cells))


inp = []
for line in open('input.txt', 'r+'):
    temp = []
    for c in line.strip():
        temp.append(c)
    inp.append(temp)
    print(temp)


for x, line in enumerate(inp):
    for y, c in enumerate(line):
        if c == "S":
            curloc = (x, y)
print(curloc)

m = [[-1 for x in range(len(inp[0]))] for _ in range(len(inp))]
checked_cells = [[-1 for x in range(len(inp[0]))] for _ in range(len(inp))]

walk(curloc, "", 0)
print(tabulate(m))
with open("out.txt", 'w+') as f:
    f.write(tabulate(m))

fill()
with open("out2.txt", 'w+') as f:
    f.write(tabulate(checked_cells))
    # for line in m:
    #     print(*line)
    # for line in m:
    #     for c in line:
    #         f.write(f"[{c}\t]")
    #     f.write("\n")
# for line in m:
#     print(line)


# 1300 too hgih
# 600 too hgih
