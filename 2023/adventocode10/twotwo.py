"""
- | is a vertical pipe connecting north and south.
- - is a horizontal pipe connecting east and west.
- L is a 90-degree bend connecting north and east.
- J is a 90-degree bend connecting north and west.
- 7 is a 90-degree bend connecting south and west.
- F is a 90-degree bend connecting south and east.
- . is ground; there is no pipe in this tile.
- S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.
"""


import os
import sys
from tabulate import tabulate
sys.setrecursionlimit(50000)



m = [[]]


def walk(coords: tuple, dir: str, distance: int):
    if distance == sys.getrecursionlimit()-1:
        print("MAXRECURSION")
        return
    if m[coords[0]][coords[1]] == distance or m[coords[0]][coords[1]] == distance +1:
        print("FINALDIST", distance )
        return
    curloc = inp[coords[0]][coords[1]]
    if curloc == ".":
        return
    # print("walking: ", coords, distance, dir, curloc)
    m[coords[0]][coords[1]] = distance
    if curloc == "S" and distance == 0:
        walk((coords[0]+1, coords[1]), "south", distance +1)
        walk((coords[0], coords[1]+1), "east", distance +1)
        walk((coords[0]-1, coords[1]), "north", distance +1)
        walk((coords[0], coords[1]-1), "west", distance +1)
    if curloc == "-":
        if dir == "east":
            walk((coords[0], coords[1]+1), "east", distance + 1)
        else:
            walk((coords[0], coords[1]-1), "west", distance +1)
    if curloc == "|":
        if dir == "north":
            walk((coords[0]-1, coords[1]), "north", distance +1 )
        else:
            walk((coords[0]+1, coords[1]), "south", distance +1)
    if curloc == "L":
        if dir == "south":
            walk((coords[0], coords[1]+1),"east", distance + 1)
        elif dir == "west":
            walk((coords[0]-1, coords[1]),"north", distance + 1)
    elif curloc == "J":
        if dir == "south":
            walk((coords[0], coords[1]-1),"west", distance + 1)
        elif dir == "east":
            walk((coords[0]-1, coords[1]),"north", distance + 1)
    elif curloc == "7":
        if dir == "north":
            walk((coords[0], coords[1]-1),"west", distance + 1)
        elif dir == "east":
            walk((coords[0]+1, coords[1]),"south", distance + 1)
    elif curloc == "F":
        if dir == "north":
            walk((coords[0], coords[1]+1),"east", distance + 1)
        elif dir == "west":
            walk((coords[0]+1, coords[1]),"south", distance + 1)
        
def check_cells():
    count = 0
    for x in range(0, len(inp)):
        for y in range(0, len(inp[0])):
            if m[x][y] != -1:
                continue
            crosses = 0 
            x2, y2 = x,y
            print(f"Starting: {x2}, {y2}, {m[x2][y2]}")
            while x2 < len(inp) and y2 < len(inp[0]):
                cur =  inp[x2][y2]
                print(f"CUR: {cur}, {m[x2][y2]}, {x2}, {y2}")
                if m[x2][y2] != -1 and cur != 'L' and cur != '7':
                    crosses +=1 
                x2 += 1
                y2 += 1
            print(crosses)
            if crosses %2 == 1:
                count +=1
                checked_cells[x][y] = "K"
    print(count)
        
    
def check_sides():
    for x in range(len(inp)-1, -1, -1):
        for y in range(len(inp[0])-1, -1, -1):
            if checked_cells[x][y] == 'K' and (x == len(inp)-1 or y == len(inp[0])-1):
                print("CHANING")
                checked_cells[x][y] = '-2'

    for x in range(len(inp)-1, -1 ,-1):
        for y in range(len(inp[0])-1, -1, -1):
            for i in range(-1, 2):
                for j in range(-1, 2):
                    if 0 <= x+i < len(inp) and 0 <= y+j < len(inp[0]):
                        if checked_cells[x][y] == 'K' and (checked_cells[x+i][y+j] == '-2' or checked_cells[x+i][y+j] == '-1') :
                            print("CHANING neghibordss")
                            checked_cells[x][y] = '-2'



inp = []
for line in open('input.txt', 'r+'):
    temp = []
    for c in line.strip():
        temp.append(c)
    inp.append(temp)
    print(temp)


for x, line in enumerate(inp):
    for y, c in enumerate(line):
        if c == "S":
            curloc = (x, y)
print(curloc)
m = [[-1 for x in range(len(inp[0]))] for _ in range(len(inp))]

walk(curloc, "", 0)
with open("out.txt", 'w+') as f:
    for line in m:
        f.write(str(line) + "\n")
print(tabulate(m))
checked_cells = [[-1 for _ in range(len(inp[0]))] for _ in range(len(inp))]
check_cells()
print(tabulate(m))
print(tabulate(checked_cells))
# check_sides()
# print(tabulate(m))
# print(tabulate(checked_cells))
with open("out2.txt", 'w') as f:
    for line in checked_cells:
        f.write(str(line) + "\n")
print(sum(x.count('K') for x in checked_cells))
# for line in m:
#     print(line)


# 938 too high



