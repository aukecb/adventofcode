"""
- | is a vertical pipe connecting north and south.
- - is a horizontal pipe connecting east and west.
- L is a 90-degree bend connecting north and east.
- J is a 90-degree bend connecting north and west.
- 7 is a 90-degree bend connecting south and west.
- F is a 90-degree bend connecting south and east.
- . is ground; there is no pipe in this tile.
- S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.
"""


import os
import sys
sys.setrecursionlimit(50000)



m = [[]]


def walk(coords: tuple, dir: str, distance: int):
    if distance == sys.getrecursionlimit()-1:
        print("MAXRECURSION")
        return
    if m[coords[0]][coords[1]] == distance or m[coords[0]][coords[1]] == distance +1:
        print("FINALDIST", distance )
        return
    curloc = inp[coords[0]][coords[1]]
    if curloc == ".":
        return
    print("walking: ", coords, distance, dir, curloc)
    m[coords[0]][coords[1]] = distance
    if curloc == "S" and distance == 0:
        walk((coords[0]+1, coords[1]), "south", distance +1)
        walk((coords[0], coords[1]+1), "east", distance +1)
        # walk((coords[0]-1, coords[1]), "north", distance +1)
        # walk((coords[0], coords[1]-1), "west", distance +1)
    if curloc == "-":
        if dir == "east":
            walk((coords[0], coords[1]+1), "east", distance + 1)
        else:
            walk((coords[0], coords[1]-1), "west", distance +1)
    if curloc == "|":
        if dir == "north":
            walk((coords[0]-1, coords[1]), "north", distance +1 )
        else:
            walk((coords[0]+1, coords[1]), "south", distance +1)
    if curloc == "L":
        if dir == "south":
            walk((coords[0], coords[1]+1),"east", distance + 1)
        elif dir == "west":
            walk((coords[0]-1, coords[1]),"north", distance + 1)
    elif curloc == "J":
        if dir == "south":
            walk((coords[0], coords[1]-1),"west", distance + 1)
        elif dir == "east":
            walk((coords[0]-1, coords[1]),"north", distance + 1)
    elif curloc == "7":
        if dir == "north":
            walk((coords[0], coords[1]-1),"west", distance + 1)
        elif dir == "east":
            walk((coords[0]+1, coords[1]),"south", distance + 1)
    elif curloc == "F":
        if dir == "north":
            walk((coords[0], coords[1]+1),"east", distance + 1)
        elif dir == "west":
            walk((coords[0]+1, coords[1]),"south", distance + 1)
    

inp = []
for line in open('input.txt', 'r+'):
    temp = []
    for c in line.strip():
        temp.append(c)
    inp.append(temp)
    print(temp)


for x, line in enumerate(inp):
    for y, c in enumerate(line):
        if c == "S":
            curloc = (x, y)
print(curloc)
m = [[-1 for x in range(len(inp))] for _ in range(len(inp))]

walk(curloc, "", 0)
with open("out.txt", 'w+') as f:
    for line in m:
        f.write(str(line) + "\n")
# for line in m:
#     print(line)



