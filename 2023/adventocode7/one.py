inp = [line.strip() for line in open('input.txt')]

cards_bids = [[cards, int(bids)] for cards,bids in (line.split() for line in inp)]


scores = {
        5: 7, #five of a kind
        4: 6, # Four of a kind
        3: 4, # Three of a kind
        2: 2, # one pair
        1: 1  # High card
        }

card_map = {
        'A': 14,
        'K': 13,
        'Q': 12,
        'J': 11,
        'T': 10,
        '9': 9,
        '8': 8,
        '7': 7,
        '6': 6,
        '5': 5,
        '4': 4,
        '3': 3,
        '2': 2,
        '1': 1,
        '0': 0
        }

score_list = []
rank = len(cards_bids)

for cards, bid in cards_bids:
    sorted_list = sorted(cards, key=cards.count, reverse=True)
    first_count = sorted_list.count(sorted_list[0])
    if first_count == 5:
        score_list.append([cards, bid, scores[first_count]])
        continue
    second_count = sorted_list.count(sorted_list[first_count])
    if first_count == 2 and second_count == 2:
        score_list.append([cards, bid, 3])
    elif second_count > 1:
        score_list.append([cards, bid, 5])
    else:
        score_list.append([cards, bid, scores[first_count]])

score_list = sorted(score_list, key=lambda card: card[2], reverse=True)
# print(score_list[:30])
score_map = []
print(rank)

same_score = {score[2]: [] for score in score_list}
for i in range(len(score_list)):
    if isinstance(same_score[score_list[i][2]], list):
        same_score[score_list[i][2]].append(score_list[i])

# print(same_score.items())

def place_in_list(l, item, index):
    if l == []:
        l.append(item)
        return l
    for x in range(len(l)-1):
        for index in range(5):
            if len(l) == 1:
                if card_map[l[0][0][index]] > card_map[item[0][index]]:
                    l.append(item)
                else:
                    print(l)
                    print("inerserting at start")
                    l.insert(0, item)
                return l
            # print(l[x][0][index], item[0][index])
            if card_map[item[0][index]] < card_map[l[0][0][index]]:
                l.append(item)
                break
            if card_map[item[0][index]] > card_map[l[-1][0][index]]:
                l.insert(0,item)
                break


            if card_map[l[x][0][index]] < card_map[item[0][index]] and card_map[l[x+1][0][index]] > card_map[item[0][index]]:
                l.insert(x, item)
                break
            elif card_map[l[x][0][index]] > card_map[item[0][index]] and card_map[l[x+1][0][index]] < card_map[item[0][index]]:
                l.append(item)
                break
    return l

end_list = []
print(len(same_score.values()))
for cards in same_score.values():
    tmp_list = []
    print(cards)
    for x in range(1, len(cards)):
        if tmp_list == []:
            tmp_list.append(cards[0])
        # print(place_in_list(tmp_list, cards[x], i))
        ll = len(tmp_list)
        print(cards[x])
        # print(tmp_list)
        tmp_list = place_in_list(tmp_list, cards[x], i)
        if ll != tmp_list:
            break
        print(tmp_list)
    end_list.extend(tmp_list)
    if len(cards) == 1:
        end_list.extend(cards)
    # print(end_list)
    if cards[0][2] == 5:
        exit()


# print(end_list)

rank = len(cards_bids)
# print(end_list)
for hand in end_list:
    hand.append(rank)
    rank -= 1

print(end_list)
total = 0
for item in end_list:
    # print(item)
    # print(item[1]*item[3])
    # print(type(item[1]*item[3]))
    total += item[1]*item[3]
print(total)





# too high 248183779
# too low 186714814
# too high 248165059
