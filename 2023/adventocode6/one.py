inp = open('input.txt')

inp = [line for line in inp]

times = [int(time) for time in inp[0].split(': ')[1].split()]
print(times)
dist = [int(dist) for dist in inp[1].split(': ')[1].split()]
print(dist)

t_sols = 1
sols = 0
for t_ind, time in enumerate(times):
    print(t_ind, dist[t_ind],time)
    sols = 0
    # time = 7, ind =1 
    for ind in range(0, time+1):
        print((time-ind)*ind)
        if (time-ind)*ind > dist[t_ind]:
            sols +=1
    t_sols *= sols
print(t_sols)

sr_time = int(inp[0].split(': ')[1].replace(' ','').strip())
sr_dist = int(inp[1].split(': ')[1].replace(' ', '').strip())
print(sr_time, sr_dist)

sols = 0
for ind in range(0, sr_time+1):
    print((sr_time-ind)*ind)
    if (sr_time-ind)*ind > sr_dist:
        sols +=1
print(sols)






