import re
inp = open('input.txt')

steps = ['seed-to-soil', 'soil-to-fertilizer', 'fertilizer-to-water', 'water-to-light', 'light-to-temperature', 'temperature-to-humidity', 'humidity-to-location']

lines = [line.strip() for line in inp]

seeds = [int(seed) for seed in re.findall('\d+', lines[0])]
new_seeds = []
for ind in range(0, len(seeds), 2):
    new_seeds.append((seeds[ind], seeds[ind+1]))
# for ind in range(len(seeds)):
#     if ind % 2 == 0:
#         for x in range(seeds[ind+1]):
#             new_seeds.append(seeds[ind]+x)
#
#     print(ind)

parts = {'seed-to-soil': [], 'soil-to-fertilizer': [], 'fertilizer-to-water': [], 'water-to-light': [], 'light-to-temperature': [], 'temperature-to-humidity': [], 'humidity-to-location': []
}

curkey_index = 0
curkey = steps[curkey_index]
for line in lines[2:]:
    try:
        if steps[curkey_index+1] in line:
            curkey_index +=1
            curkey = steps[curkey_index]
    except IndexError:
        pass
    vals = re.findall('\d+', line)
    vals = [int(val) for val in vals]
    if vals != []:
        parts[curkey].append(vals)


# print(parts)
# print(parts['seed-to-soil'])


def findIndex(l, s):
    # print(l)
    for x_ind in range(0, len(l)):
        x = l[x_ind]

        if s > x[1] and s < x[1]+x[2]:
            diff = (s - x[1]) + x[0]
            return diff
    return s

# lowestLocation = 1000000000000
# for seed in seeds:
#     soil = findIndex(parts['seed-to-soil'], seed)
#     fertilizer = findIndex(parts['soil-to-fertilizer'], soil)
#     water = findIndex(parts['fertilizer-to-water'], fertilizer)
#     light = findIndex(parts['water-to-light'], water)
#     temperature = findIndex(parts['light-to-temperature'], light)
#     humidity = findIndex(parts['temperature-to-humidity'], temperature)
#     location = findIndex(parts['humidity-to-location'], humidity)
#     print(seed, soil, fertilizer, water, light, temperature, humidity, location)
#     if location < lowestLocation:
#         lowestLocation = location
# print(lowestLocation)

        
    
lowestLocation = 1000000000000
seedinfo = []

print(new_seeds, len(new_seeds))
# for ind, seed in enumerate(new_seeds):
seed = new_seeds[-1]
prev_val = 0
for x in range(seed[0]+190000000, seed[0]+seed[1], 1001):
    for y in range(len(steps)):
        if x % 1000 == 0:
            print(x, ' / ', seed[0]+seed[1])
        if y == 0:
            prev_val = findIndex(parts[steps[y]], x)
        else:
            prev_val = findIndex(parts[steps[y]], prev_val)
        if y == len(steps)-1 and prev_val < lowestLocation:
            lowestLocation = prev_val
            seedinfo = [x, y]

print(lowestLocation, seedinfo)
print(new_seeds[-1])
soil = findIndex(parts['seed-to-soil'], 426494918)
fertilizer = findIndex(parts['soil-to-fertilizer'], soil)
water = findIndex(parts['fertilizer-to-water'], fertilizer)
light = findIndex(parts['water-to-light'], water)
temperature = findIndex(parts['light-to-temperature'], light)
humidity = findIndex(parts['temperature-to-humidity'], temperature)
location = findIndex(parts['humidity-to-location'], humidity)
print(seed, soil, fertilizer, water, light, temperature, humidity, location)
print(lowestLocation)
exit()
for ind, seed in enumerate(new_seeds):
    prev_val = 0
    for y in range(len(steps)):
        if ind % 10 == 0:
            print(ind, ' / ', seed[0]+seed[1])
        if y == 0:
            prev_val = findIndex(parts[steps[y]], seed[0])
        else:
            prev_val = findIndex(parts[steps[y]], prev_val)
        if y == len(steps)-1 and prev_val < lowestLocation:
            lowestLocation = prev_val
    print(lowestLocation)

print(lowestLocation)
# lowest = 100000000000000000
# for part in parts['humidity-to-location']:
#     print(part)
#     if part[0] < lowest:
#         lowest = part[0]
# print(lowest)
# partzero = []
# for part in parts['humidity-to-location']:
#     partzero.append(part[0])
#
# print(sorted(partzero)[:10])
# print(findIndex(parts['temperature-to-humidity'], 0))
# print(parts['temperature-to-humidity'])
#
# print(lowestLocation)
#     
# 334 too low
# 1161863399 too high
# started: 3124  5.12.2023 17:59  python one.py
# ended: 5.12.2023 23:53
