import re
inp = open('input.txt')

steps = ['seed-to-soil', 'soil-to-fertilizer', 'fertilizer-to-water', 'water-to-light', 'light-to-temperature', 'temperature-to-humidity', 'humidity-to-location']

lines = [line.strip() for line in inp]

seeds = [int(seed) for seed in re.findall('\d+', lines[0])]
new_seeds = []
for ind in range(0, len(seeds), 2):
    new_seeds.append((seeds[ind], seeds[ind+1]))

parts = {'seed-to-soil': [], 'soil-to-fertilizer': [], 'fertilizer-to-water': [], 'water-to-light': [], 'light-to-temperature': [], 'temperature-to-humidity': [], 'humidity-to-location': []
}

curkey_index = 0
curkey = steps[curkey_index]
for line in lines[2:]:
    try:
        if steps[curkey_index+1] in line:
            curkey_index +=1
            curkey = steps[curkey_index]
    except IndexError:
        pass
    vals = re.findall('\d+', line)
    vals = [int(val) for val in vals]
    if vals != []:
        parts[curkey].append(vals)


# print(parts)
# print(parts['seed-to-soil'])


def findIndex(l, s):
    # print(l)
    for x_ind, x in enumerate(l):
        if s > x[1] and s < x[1]+x[2]:
            diff = (s - x[1]) + x[0]
            return diff
    return s

print(new_seeds)
print()
print(sorted(new_seeds))
sorted_seeds = sorted(new_seeds)
new_sorted_seeds = []

for seed in sorted_seeds:
    new_sorted_seeds.append((seed[0], seed[0]+seed[1]-1))
print(new_sorted_seeds)
sorted_seeds = new_sorted_seeds

new_sorted_seeds = []
for ind, (start, end) in enumerate(sorted_seeds):
    print(ind, start, end)
    try:
        if end > sorted_seeds[ind+1][0]:
            new_sorted_seeds.append((start, sorted_seeds[ind+1][1]))
        else:
            new_sorted_seeds.append((start, end))
    except IndexError:
        pass

print(parts['soil-to-fertilizer'])
for part in parts:
    print(part)
    parts[part]= sorted(parts[part], key=lambda p: p[1])

new_parts = {}

for part in parts:
    print()
    print(part)
    new_parts[part] = []
    for ind, (dest, source, r) in enumerate(parts[part]):
        print(ind, dest ,source ,r)
        if (source + r) > parts[part][ind+1][1]:
            print(" OVERLOP")


