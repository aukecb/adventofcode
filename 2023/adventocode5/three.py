inp = open("input.txt")

inp = [line for line in inp]
seeds = [int(x) for x in inp[0].split(': ')[1].split(' ')]
print(seeds)

seeds = [(seeds[x], seeds[x]+seeds[x+1]) for x in range(0, len(seeds), 2)]
print(seeds)

# funcs = [print(x) for x in (line.strip().split() for line in inp[1:])]
funcs = []
tmp = []
for line in inp[3:]:
    if ':\n' in line:
        funcs.append(tmp)
        tmp = []
    elif line == '\n':
        pass
    else:
        t = []
        for x in line.strip().split():
           t.append(int(x))
        tmp.append(t)



def apply_range(func, R):
    A = []
    for (dest, src, sz) in func:
      src_end = src+sz
      NR = []
      while R:
        # [st                                     ed)
        #          [src       src_end]
        # [BEFORE ][INTER            ][AFTER        )
        (st,ed) = R.pop()
        # (src,sz) might cut (st,ed)
        before = (st,min(ed,src))
        inter = (max(st, src), min(src_end, ed))
        after = (max(src_end, st), ed)
        if before[1]>before[0]:
          NR.append(before)
        if inter[1]>inter[0]:
          A.append((inter[0]-src+dest, inter[1]-src+dest))
        if after[1]>after[0]:
          NR.append(after)
      R = NR
    return A+R

P2 = []

for start, end in seeds:
    R = [(start, end)]
    for func in funcs:
        R = apply_range(func, R)
    print(R)
    P2.append(min(R)[0])
print(min(P2))
print(P2)


# 60568880
