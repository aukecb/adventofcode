import math

inp = [line.strip() for line in open('input.txt')]
instruction = inp[0]
print(instruction)

m = {line.split(' = ')[0]: (line.split(' = ')[1].split(', ')[0].strip('('), line.split(', ')[1].strip(')'))  for line in inp[2:]}

# n = 'AAA'
# steps = 0
# ins = ''
# ins_index = 0
# while n != 'ZZZ':
#     ins = instruction[ins_index]
#     # print(n)
#     if ins == 'L':
#         n = m[n][0]
#     else:
#         n = m[n][1]
#     steps += 1
#     ins_index += 1
#     if ins_index % len(instruction) == 0:
#         # print(n , ins_index, "resetting", steps)
#         ins_index = 0
#         # print("resetting", steps)
# print(steps)

cur_nodes = []
for node in m:
    print(node)
    if node[2] == 'A':
        cur_nodes.append(node.split()[0])
steps = 0
ins_index = 0
ins = ''
# while any(node[2] != 'Z' for node in cur_nodes ):
#     ins = instruction[ins_index]
#     if ins == 'L':
#         for i, node in enumerate(cur_nodes):
#             cur_nodes[i] = m[node][0]
#     else:
#         for i, node in enumerate(cur_nodes):
#             cur_nodes[i] = m[node][1]
#     steps += 1
#     ins_index += 1
#     if ins_index % len(instruction) == 0:
#         ins_index = 0
#         print(steps)
    # print(cur_nodes)
def solveroute(x):
    ins = ''
    index =0
    steps = 0
    while x[2] != 'Z':
        ins = instruction[index]
        print(ins, x)
        if ins == 'L':
            x = m[x][0]
        else:
            x = m[x][1]
        index += 1
        if index % len(instruction) == 0:
            index = 0
        steps += 1
    return steps


answers = []
ret = 1
for route in cur_nodes:
    answers.append(solveroute(route))
    ret = math.lcm(ret, solveroute(route))
    print(answers)
print(answers)
print(ret)

print(steps)
print(math.lcm(steps))


# 283 too low
