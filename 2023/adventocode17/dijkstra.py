import heapq

class Node:
  def __init__(self,x_coord,y_coord):
      self.d=float('inf') #current distance from source node
      self.parent=None
      self.finished=False

def dijkstra(graph,source):
  nodes={}
  for y, line in enumerate(graph):
    for x, node in enumerate(line):
        nodes[node]=Node(y, x)
  nodes[source].d=0
  queue=[(0,source)] #priority queue
  while queue:
      d,node=heapq.heappop(queue)
      if nodes[node].finished:
          continue
      nodes[node].finished=True
      for neighbor in graph[node]:
          if nodes[neighbor].finished:
              continue
          new_d=d+graph[node][neighbor]
          if new_d<nodes[neighbor].d:
              nodes[neighbor].d=new_d
              nodes[neighbor].parent=node
              heapq.heappush(queue,(new_d,neighbor))
  return nodes

inp = [[(int(x), 10000) for x in line.strip()] for line in open("testinput.txt", "r")]

inp[0][0] = (inp[0][0][0], 0)

print(inp)

dijkstra(inp, (0,0))

