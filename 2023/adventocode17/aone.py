"""
To find the optimal route, recursion can be used

r -- r 
  |- d
"""
import sys
import copy
from tabulate import tabulate

sys.setrecursionlimit(160)



inp = [[int(x) for x in line.strip()] for line in open("testinput.txt", "r")]
print(inp)
print(inp[0][1])

dir_look = {
    "r": (0,1),
    "l": (0,-1),
    "u": (-1,0),
    "d": (1,0),
}

no_dir = {
    "r": "l",
    "l": "r",
    "u": "d",
    "d": "u",
}

out_lookup = {
    "r": ">",
    "l": "<",
    "u": "^",
    "d": "v",
}

counters = [100000]

out = copy.deepcopy(inp)

def get_pos(pos, dir):
    return (pos[0]+dir_look[dir][0], pos[1]+dir_look[dir][1])

def walk(pos, dir, route, counter):
    dirs = []
    vals = []
    for i in range(4):
        if list(dir_look.keys())[i] != no_dir[dir]:
            dirs.append((get_pos(pos, list(dir_look.keys())[i]), list(dir_look.keys())[i]))
        # vals.append()

    smallest_dir = ((0,0), 100, "")
    for d, key in dirs:
        print(d)
        if inp[d[0]][d[1]] < smallest_dir[1] and 0 <= d[0] < len(inp) and 0 <= d[1] < len(inp[0])-1:
            print("checking", key, len(route))
            if len(route) >= 3: 
                if all(r == route[-1] for r in route[-2:]) and route[-1] == key:
                    print("CONITNUGUING", route[-2:])
                    continue
            if type(out[d[0]][d[1]]) == int:
                smallest_dir = (d, inp[d[0]][d[1]], key)
    print(smallest_dir, dirs, route, dir, counter)

    if smallest_dir[2] == "":
        return counter
    if pos[0] == len(inp) and pos[1] == len(inp[0]):
        print("FINISHED")
        return counter

    out[smallest_dir[0][0]][smallest_dir[0][1]] = out_lookup[smallest_dir[2]]
    
    # if counter % 10 == 0:
    print(tabulate(out))

    walk(smallest_dir[0], smallest_dir[2], route +smallest_dir[2], counter + smallest_dir[1])
    # # r_pos = get_pos(pos, 'r')
    # # l_pos = get_pos(pos, 'l')
    # # u_pos = get_pos(pos, 'u')
    # # d_pos = get_pos(pos, 'd')

    # r_count = counter + inp[r_pos[0]][r_pos[1]]

walk((0,0), 'r', "", 0)