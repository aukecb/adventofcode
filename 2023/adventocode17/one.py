import sys

sys.setrecursionlimit(5000000)


dir_look = {
    "r": (0,1),
    "l": (0,-1),
    "u": (-1,0),
    "d": (1,0),
}

traceroute = {
    
}

totalcounter = [1000, 999,992]
print(totalcounter)

inp = []
inp = [line.strip() for line in open("testinput.txt", "r")]


pos = (0,0)
counter = 0
dir = "r"


def get_pos(pos, dir):
    return (pos[0]+dir_look[dir][0], pos[1]+dir_look[dir][1])

def walk(pos, dir, route, counter):
    traceroute.pop(route[:-1], None)
    traceroute[route] = counter
    # print(traceroute)
    if len(route) >=2:
        last = route[-1]
        if all(c == last for c in route[-3:]):
            # print(route)
            return counter
    r_pos = get_pos(pos, "r")
    l_pos = get_pos(pos, "l")
    u_pos = get_pos(pos, "u")
    d_pos = get_pos(pos, "d")
    # if counter > 100:
    #     return -1

    # if counter > totalcounter[0]:
    #     return -1

    # if counter < totalcounter[0]:
    #     totalcounter[0] = counter

    if pos[0] == len(inp)-1 and pos[1] == len(inp[0])-1:
        print("RETURNGIN COUTNER")
        totalcounter = totalcounter.sort()
        print(totalcounter)
        return counter
    
    if not(0 <= pos[0] <= len(inp)-1 and 0 <= pos[1] <= len(inp[0])-1):
        return -1

    counter += int(inp[pos[0]][pos[1]])
    # print(pos, dir, route, counter, r_pos, l_pos, u_pos, d_pos)
    if dir == "r":
        walk(r_pos, "r", route+"r", counter)
        walk(u_pos, "u", route+"u", counter)
        walk(d_pos, "d", route+"d", counter)
    if dir == "l":
        walk(l_pos, "l", route+"l", counter)
        walk(u_pos, "u", route+"u", counter)
        walk(d_pos, "d", route+"d", counter)
    if dir == "u":
        walk(r_pos, "r", route+"r", counter)
        walk(u_pos, "u", route+"u", counter)
        walk(d_pos, "l", route+"l", counter)
    if dir == "d":
        walk(r_pos, "r", route+"r", counter)
        walk(u_pos, "l", route+"l", counter)
        walk(d_pos, "d", route+"d", counter)

# for y in range(0, len(inp)):
#     for x in range(0, len(inp[0])):
#         s_loc = (10,10)
#         smallest = 10
#         for i in range(-1, 2):
#             for j in range(-1, 2):
#                 if 0 <= pos[1]+i < len(inp) and 0 <= pos[0]+j < len(inp[0]):
#                     if (i == 0 and j ==0) or (abs(i) == 1 and abs(j) == 1):
#                         continue
#                     if int(inp[pos[1]+i][pos[0]+j]) < int(smallest):
#                         smallest = inp[pos[1]+i][pos[0]+j]
#                         # s_loc = (pos[1]+i, pos[0]+j)
#                         s_loc = (i, j)
                
#         if counter % 3 == 0:
#             # turn direction
#             pass

#         print(s_loc)
#         dir = [dir for dir, coord in dir_look.items() if coord == s_loc][0]
#         print(dir)
#         pos = (pos[0]+dir_look[dir][0], pos[1]+dir_look[dir][1])
#         counter += 1
#         print(inp[y][x], end="")
# print(counter)

walk((0,0), "r", "", 0)
print(totalcounter.sort())
print(totalcounter)
# walk((0,0), "d", "")
# walk((0,0), "r", "")