inp = open("input.txt")

colors = ["red", "blue", "green"]
maxColors = {"red": 12, "green": 13, "blue": 14}
validGames = []
powerOfGames = []

for ind, line in enumerate(inp):
    l = line.split(' ')
    l = [item.strip().replace(',', '').replace(':', '') for item in l]

    gameIndex = l[1]
    l = l[2:]
    l[-1]= l[-1] + ';'
    tmp = []
    new_l = []
    last_ind = 0
    for ind, item in enumerate(l):
        findind = item.find(';')
        if findind != -1:
            l[ind] = item.replace(';', '')
            new_l.append(l[last_ind:ind+1])
            last_ind = ind+1
    print(new_l)
    
    validity = True
    highest = {}
    highest["red"] = 0
    highest["blue"] = 0
    highest["green"] = 0
    for seq in new_l:
        seqd = {}
        for i, item in enumerate(seq):
            if i % 2:
                seqd[item] = int(seq[i-1])
                if int(seq[i-1]) > int(highest[item]):
                    highest[item] = seq[i-1]
        print(seqd)
        for key in seqd:
            if seqd[key] > maxColors[key]:
                validity = False
    if validity:
        validGames.append(int(gameIndex))
    print("HIGHTEST", highest)
    mul_calc = 1
    for val in highest.values():
        mul_calc *= int(val)

    print(mul_calc)
    powerOfGames.append(mul_calc)

print(validGames)
print(sum(validGames))
print(powerOfGames)
print(sum(powerOfGames))
