inp = open("input.txt")

lookup = {'one': 'o1e', 'two': 't2o', 'three': 't3e', 'four': 'f4r', 'five': 'f5e', 'six': 's6x', 'seven': 's7n', 'eight': 'e8t', 'nine': 'n9e'}

inp2 = []
for line in inp:
    for word in lookup.keys():
        if word in line:
            line = line.replace(word, lookup[word])
            # inp2.append(line.replace(word, lookup[word]))
            # print(line)
    inp2.append(line)


# for line in inp2:
#     print(line)
digit_list = []
for line in inp2:
    tmp_l = []
    for char in line:
        if char.isdigit():
            tmp_l.append(char)
    print(line)
    item = tmp_l[0] + tmp_l[-1]
    print(item)
    # print(item)
    digit_list.append(int(item))
#     print(digit_list)
# print(digit_list)
print(sum(digit_list))

