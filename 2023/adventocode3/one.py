import re
inp = open("input.txt")

lines= [i.strip() for i in inp]


def findNumber(inn):
    # print(re.findall(r'\d+', inn))
    nums = re.findall(r'[0-9]{1,3}', inn)
    ret = [(m.span()[0], int(m.group())) for m in re.finditer(r'[0-9]{1,3}', inn)]
    # print(ret)
    return ret
    

totalSum = 0
totalGear = 0
cur_gear = {}
for x, line in enumerate(lines):
    nums = findNumber(line)
    # print(nums)
    for index, num in nums:
        # print("CHECKING", index, num)
        added = False
        index = index-1
        l_mod = len(str(num))
        for mod in range(-1, 2):
            for l in range(l_mod +2):
                try:
                    # print("CHECKING", index, x+mod, index+l, lines[x+mod][index+l])
                    if lines[x+mod][index+l] != '.' and \
                            not lines[x+mod][index+l].isdigit() and not added:
                        if lines[x+mod][index+l] == '*' and not added:
                            
                            if str(x+mod) + str(index+l) in cur_gear:
                                cur_gear[str(x+mod) + str(index+l)].append(num)
                            else:
                                cur_gear[str(x+mod) + str(index+l)] = [num]
                            added = True
                        # print("ADDING", num)
                        totalSum += int(num)
                        added = True
                except IndexError:
                    pass

print(totalSum)
for vals in cur_gear.values():
    if len(vals) > 1:
        totalGear += vals[0] * vals[1]
print(totalGear)

#485153 too low
#483410 too low
#485153 too low
#492100
#536576 right ANSER
