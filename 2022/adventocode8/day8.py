f = open("input.txt", "r")
inp = f.read().splitlines()

countvis = 0
def part_one():
    for y, line in enumerate(inp):
            

        for x, height in enumerate(line):
            docheck = True
            if x== 0 and  y == 0:
                countvis += (2 *(len(inp) -1)) + (2*(len(line)-1))
                docheck = False
            if x == 0:
                docheck = False
            if y == 0:
                docheck = False
            if x == len(line)-1 or y == len(inp)-1:
                docheck = False
            highest = [0, 0, 0, 0]
            if docheck:
                print(x,y,height)
                for c in range(0, y):
                    if int(inp[c][x]) > highest[2]:
                        highest[2] = int(inp[c][x])
                for c in range(len(inp)-1, y , -1):
                    if int(inp[c][x]) > highest[3]:
                        highest[3] = int(inp[c][x])

                for c in range(0,x):
                    if int(line[c]) > highest[0]:
                        highest[0] = int(line[c])
                for c in range(len(line)-1, x, -1):
                    if int(line[c]) > highest[1]:
                        highest[1] = int(line[c])
                viz = False
                print(highest)
                for elem in highest:
                    if elem < int(line[x]):
                        viz = True
                print(viz)
                if x != 0 and x != len(inp) and y != 0 and y != len(line) and viz:
                    countvis += 1

        
            
def part_two():
    totalviz = 1
    highestscenic = 0
    for y, line in enumerate(inp):
        for x, height in enumerate(line):
            viztrees = 1
            print(x,y,height)
            v = 0
            for c in range(y, 0, -1):
                # print(inp[c][x], c)
                if int(inp[c-1][x]) > int(inp[y][x]):
                    v += 1
                    break
                elif int(inp[c-1][x]) == int(inp[y][x]):
                    v += 1
                    break
                else:
                    v += 1
            print("1", v)
            if v != 0:
                viztrees *= v
            v = 0
            for c in range(y, len(inp)-1):
                if int(inp[c+1][x]) > int(inp[y][x]):
                    v += 1
                    break
                elif int(inp[c+1][x]) == int(inp[y][x]):
                    v += 1
                    break
                else:
                    v += 1
            print("2", v)
            if v != 0:
                viztrees *= v
            v = 0
            for c in range(x,0, -1):
                if int(line[c-1]) > int(inp[y][x]):
                    v += 1
                    break
                elif int(line[c-1]) == int(inp[y][x]):
                    v+=1
                    break
                else:
                    v += 1
            print("3", v)
            if v != 0:
                viztrees *= v
            v = 0
            for c in range(x, len(line)-1):
                if int(line[c+1]) > int(inp[y][x]):
                    v +=1 
                    break
                elif int(line[c+1]) == int(inp[y][x]):
                    v += 1
                    break
                else:
                    v += 1
            
            print("4", v)
            viztrees *= v
            print(viztrees)
            if viztrees > highestscenic:
                highestscenic = viztrees

    return highestscenic
    # print(totalviz)


print(part_two())
print(countvis)
