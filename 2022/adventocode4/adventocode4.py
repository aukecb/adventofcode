f = open("./adventocode4.txt", "r")
inp = f.read().splitlines()
print(inp)

def create_range(r):
    ret = []
    r = r.split("-")
    print(r)
    for i in range(int(r[0]), int(r[1])+1):
        ret.append(i)
    return ret

totalcount = 0
count2 = 0
for pair in inp:

    p = pair.split(",")
    p1 = create_range(p[0])
    p2 = create_range(p[1])
    if all(e in p1 for e in p2):
        print(f"YEs:\t{p1}\t{p2}")
        totalcount += 1

    elif all(e in p2 for e in p1):
        print("ALSOYES:\t{p2}\t{p1}")
        totalcount += 1


    if any(e in p1 for e in p2):
        count2 += 1
    elif any(e in p2 for e in p1):
        count2 += 1

print(totalcount)
print(count2)


