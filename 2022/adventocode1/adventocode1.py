import os


def get_input():
    f = open("./adventocode1.txt", "r")
    elves = f.read().split("\n\n")
    #elves = [[int(i.split('\n')[0]) for i in elves]]
    #elves = elves[:len(elves)-1]
    print(elves)
        
#    elflist = []
#    for elf in elves:
#        templist = []
#        print(elf)
#        for cal in elf.split('\n'):
#            print(cal)
#            try:
#                templist.append(int(cal))
#            except ValueError:
#                pass
#            elflist.append(templist)
    for index, elf in enumerate(elves):
        elf = elf.split("\n")
        if '' in elf:
            elf.remove('')
        print(elf)
        elves[index] = elf
    elves = [[int(i.replace("\n", '')) for i in elf] for elf in elves]


    print(elves)
    return elves


def part_one(elves):
    highest = (0, -1)
    for index, elf in enumerate(elves):
        if sum(elf) > highest[1]:
            highest = (index, sum(elf))
        
    print(highest)

def part_two(elves, ranking=3):
    highest = [] 
    for i in range(0, ranking):
        highest.append(0)

    for index, elf in enumerate(elves):
        s = sum(elf)
        print(f"{index}\t{s}\t{elf}")
        for i in range(0, len(highest)):
            if s > highest[i]:
                print(highest)
                highest[i] = s
                highest.sort()
                print(highest)
                break
    print(highest)
    total = 0
    for val in highest:
        total += val
    print(total)



def main():
    e = get_input()
    part_one(e)
    part_two(e)


if __name__ == "__main__":
    main()

#203731
