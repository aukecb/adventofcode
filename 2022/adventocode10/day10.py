inp = open('input.txt', 'r').read().splitlines()

print(inp)
print("\n\n")


cycles = 0
X = 1
vals = {}

def part_one():
    for ins in inp:
        cycles += 1
        # print(cycles)
        if (cycles +20) % 40 == 0:
            print(cycles, X, cycles * X)
            vals[cycles] = cycles * X
        ins = ins.split(' ')
        if 'addx' in ins[0]:
            cycles += 1
            if (cycles +20) % 40 == 0:
                print(cycles, X, cycles * X)
                vals[cycles] = cycles * X
            X += int(ins[1])
    totalstrength = 0
    print(vals)
    for key, val in vals.items():
        totalstrength += val
    print(totalstrength)

def part_two():
    cycles = 0
    ind = 0
    X = 1
    vals = {}
    CRT = ""
    for ins in inp:
        cycles += 1
        if ind == X or ind == X-1 or ind == X+1:
            CRT += "#"
        else:
            CRT += "."
        ind +=1 
        print(cycles, X)
        # print(cycles)
        ins = ins.split(' ')
        if ind > 39:
            CRT += "\n"
            ind = 0
        if 'addx' in ins[0]:
            cycles += 1
            if ind == X or ind == X-1 or ind == X+1:
                CRT += "#"
            else:
                CRT += "."
            ind +=1 
            X += int(ins[1])
        if ind > 39:
            CRT += "\n"
            ind = 0
        print(CRT)


# part_one()
l = part_two()

