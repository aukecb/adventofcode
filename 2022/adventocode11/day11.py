import copy
from math import lcm
inp = open("input.txt", "r").read().split('\n\n')

print(inp)


monkeylist = []
for ind, m in enumerate(inp):
    monk = []
    print(m)
    m = m.split('\n')
    items = m[1].split(': ')[1].split(', ')
    items = [int(item) for item in items]
    print(items)
    monk.append(m[2].split('= ')[1])
    test = [int(m[3].split('divisible by ')[1]), int(m[4].split(' ')[-1]), int(m[5].split(' ')[-1])]
    monk.append(test)
    monk.insert(0, items)
    monkeylist.append(monk)

print(monkeylist)

def do_op(item, m):
    # print(m[1])
    val = m[1].split(' ')[-1]
    if val == "old":
        val = item
    else :
        val = int(val)
    match(m[1].split(' ')[1]):
        case "+":
            return item + val
        case "-":
            return item - val
        case "*":
            return item * val
        case "/":
            return item / val

def do_test(item, m):
    if item % m[2][0] == 0:
        return m[2][1]
    else:
        return m[2][2]

            
divisors = []
for m in monkeylist:
    divisors.append(m[2][0])
LCM = lcm(*divisors)
print(LCM)

def get_inspection(ml, divbythree, rounds):
    cinspect = [0] * len(inp)
    for r in range(0, rounds):

        for ind, m in enumerate(ml):
            for item in list(m[0]):
                cinspect[ind] += 1
                op = do_op(item, m)

                # op = int((op /3) // 1)
                if divbythree:
                    op = int((op/3) //1)
                    test = do_test(op, m)
                else:
                    test = do_test(op, m)
                    op = op % LCM
                # test2 = do_test(op, ml[test])
                # if test2 == ml[test][2][1]:
                    # op = ml[test][2][0]
                # else:
                    # op = ml[test][2][0] +1
                # print(ml[test])
                # print("OP: ", op)
                ml[test][0].append(op)
                m[0].pop(0)
    return cinspect



l = copy.deepcopy(monkeylist)
ins = get_inspection(l, False, 10000)
print(sorted(ins)[-1] * sorted(ins)[-2])
ins = get_inspection(copy.deepcopy(monkeylist), True, 20)
print(sorted(ins)[-1] * sorted(ins)[-2])
# print(cinspect)
# print(sorted(cinspect)[-1] * sorted(cinspect)[-2])
