inp = open("input.txt", "r").read().splitlines()


print(inp)
def part_one():
    headpos = [0,0]
    tailpos = [0,0]
    count = 0
    poshistory = []
    for line in inp:
        direction, amount = line.split(" ")
        amount = int(amount)
        for a in range(0, amount):
            match direction:
                case "U":
                    headpos[1] -= 1
                case "D":
                    headpos[1] += 1
                case "L":
                    headpos[0] -= 1
                case "R":
                    headpos[0] += 1
            print(headpos, tailpos)


            # print(poshistory)
            if abs(tailpos[0] - headpos[0]) > 1 :
                if tailpos[0] != headpos[0] and tailpos[1] != headpos[1]:
                    print("DIAGONELA")
                    if (headpos[0] - tailpos[0]) > 0:
                        tailpos[0] += 1
                    else:
                        tailpos[0] -= 1

                    if (headpos[1] - tailpos[1]) > 0:
                        tailpos[1] += 1
                    else:
                        tailpos[1] -= 1
                elif direction == "L":
                    tailpos[0] -=1
                else:
                    tailpos[0] += 1

            if abs(tailpos[1] - headpos[1]) > 1:
                if tailpos[0] != headpos[0] and tailpos[1] != headpos[1]:
                    if (headpos[0] - tailpos[0]) > 0:
                        tailpos[0] += 1
                    else:
                        tailpos[0] -= 1

                    if (headpos[1] - tailpos[1]) > 0:
                        tailpos[1] += 1
                    else:
                        tailpos[1] -= 1

                elif direction == "U":
                    tailpos[1] -=1
                else:
                    tailpos[1] += 1

            if tailpos not in poshistory:
                poshistory.append(list(tailpos))
                count += amount



def follow_head(hp, tp, direction):
    # print(hp, tp)
    if abs(tp[0] - hp[0]) > 1 :
        if tp[0] != hp[0] and tp[1] != hp[1]:
            if (hp[0] - tp[0]) > 0:
                tp[0] += 1
            else:
                tp[0] -= 1

            if (hp[1] - tp[1]) > 0:
                tp[1] += 1
            else:
                tp[1] -= 1
        elif hp[0] -tp[0] > 0:
            tp[0] += 1
        else:
            tp[0] -=1

    if abs(tp[1] - hp[1]) > 1:
        if tp[0] != hp[0] and tp[1] != hp[1]:
            if (hp[0] - tp[0]) > 0:
                tp[0] += 1
            else:
                tp[0] -= 1

            if (hp[1] - tp[1]) > 0:
                tp[1] += 1
            else:
                tp[1] -= 1

        elif hp[1] - tp[1] > 0:
            tp[1] += 1
        else:
            tp[1] -=1

    


def part_two():
    hp = [0,0]
    tp = []
    for i in range(0,9):
        tp.append([0,0])
    print(tp)
    count = 0
    poshistory = []

    for line in inp:
        direction, amount = line.split(" ")
        amount = int(amount)
        for a in range(0, amount):
            match direction:
                case "U":
                    hp[1] -= 1
                case "D":
                    hp[1] += 1
                case "L":
                    hp[0] -= 1
                case "R":
                    hp[0] += 1
            follow_head(hp, tp[0], direction)
            for i in range(0, len(tp)-1):
                follow_head(tp[i], tp[i+1], direction)
            if tp[8] not in poshistory:
                poshistory.append(list(tp[8]))
                count += amount
        print(line, hp, tp )

    return poshistory



# poshistory.pop(0)
print(len(part_two()))
