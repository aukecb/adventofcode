f = open("input.txt", "r")
inp = f.read().splitlines()

# print(inp)



def check_dir(path, d):
    if path[0] not in d[0].keys():
        d[0][path[0]] = [{}]
        # print("D:\t", d)
        return d
    if len(path) == 1:
        return d
    check_dir(path[1:], d[0][path[0]])

def add_dir(path, d, dirs):
    if len(path) ==1:
        d[path[0]] = dirs
        return d
    add_dir(path[1:], d[path[0]], dirs)

def create_dict(path, dirs):
    return dict((el, {}) for el in dirs)


fs = {}
curpath = []
for ind, line in enumerate(inp):
    l = line.split(" ")
    
    if l[0] == "$":
        if l[1] == "cd":
            if l[2] == "..":
                curpath = curpath[:-1]
            else:
                curpath.append(l[2])
            # print("CURPATH:\t", curpath)
        elif l[1] == "ls":
            dirs = []
            dirs = {}
            for i in range(ind+1, len(inp)):
                if inp[i].split(" ")[0] == "$":
                    break
                if inp[i].split(" ")[0] == "dir":
                    dirs[inp[i].split(" ")[1]] = {}
                    # dirs.append(inp[i].split(" ")[1])
                else:
                    dirs[inp[i].split(" ")[1]] = int(inp[i].split(" ")[0])
            add_dir(curpath, fs, dirs)
            # print(fs)


res = []
def get_size(d):
    # print(d)
    dirsize = 0
    for key, val in d.items():
        print(key, val)
        if isinstance(val, dict):
            d[key] = get_size(val)
            if d[key] > 3562874:
                res.append((key, d[key]))
        val = d[key]
        if isinstance(val, int):
            dirsize += val
    # print(d.keys())
    # print(d)
    # d[list(d.keys())[0]] = dirsize
    # print(d)
    s = 0
    for key, val in d.items():
        s += val
    if s > 999999:
        print(s)

    return dirsize

fd = get_size(fs)
print(fd)
print(res)
s = 0
for r in res:
    s += r[1]
print(s)
# print(d for d in fd)
            # print("LS:\t", dirs)


totalused = fd
totalspace = 70000000
update = 30000000
mustbefreed = update -(totalspace -totalused) 
print(totalused)
print(mustbefreed)
res.sort(key=lambda a: a[1])
print(res[0])

#20338342 too high
#43562874 too high

