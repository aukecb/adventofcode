f = open("./adventocode3.txt", "r")

#for line in f.read().splitlines():
#    print(line)
#    print(len(line))
#    print(int(len(line)/2))

inp = f.read().splitlines()

bp = [[line[:int(len(line)/2)], line[int(len(line)/2):]] for line in inp]
print(bp)


def get_priority(letter:chr):
    if letter.islower():
        return ord(letter) -96
    else:
        return ord(letter)-38


one = [get_priority((set(first) & set(second)).pop()) for first, second in bp]

print(sum(one))

bp2 = inp

two = [get_priority(set.intersection(*[set(s) for s in inp[i:i+3]]).pop()) for i in range(0, len(inp), 3)]
print(sum(two))

