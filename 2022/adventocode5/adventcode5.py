# f = open("adventocode5.txt", "r")
f = open("adventocode5_large_input_8.txt", "r")
inp = f.read().splitlines()
##print(inp)
crates = inp[:inp.index('')]
instr = inp[inp.index('') + 1:]
##print(crates)
##print(instr)


def load_config(inp):
    cratelist = {}
    index = 0
    for line in inp:
        for i, letter in enumerate(line):
            if letter == "[":
                if int(i/4)+1 in cratelist:
                    cratelist[int(i/4)+1].append(line[i+1])
                else:
                    cratelist[(int(i/4))+1] = list(line[i+1])
    cratelist = dict(sorted(cratelist.items()))
    #print(cratelist)
    return cratelist


def move_crates(cratelist, instr):
    ins = [(int(x) for x in s if x.isdigit()) for s in instr ]
    inslist = []
    for ins in instr:
        templist = []
        i = ins.split(" ")
        inslist.append([int(i[1]), int(i[3]), int(i[5])])
     
    for amount, orig, dest in inslist:
        for i in range(0, amount):
            temp = cratelist[orig].pop(0)
            cratelist[dest].insert(0, temp)
    return cratelist

def move_crates2(cratelist, instr):
    ins = [(int(x) for x in s if x.isdigit()) for s in instr ]
    inslist = []
    for ins in instr:
        templist = []
        i = ins.split(" ")
        inslist.append([int(i[1]), int(i[3]), int(i[5])])
     
    for amount, orig, dest in inslist:
        temp = []
        for i in range(0, amount):
            temp.append(cratelist[orig].pop(0))
            #print(temp)

        cratelist[dest] = temp + cratelist[dest]
        #print(cratelist)
    return cratelist
    



crateconfig = load_config(crates)
#final = move_crates(crateconfig, instr)
final2 = move_crates2(crateconfig, instr)
res1 = ""
res2 = ""
#for key in final:
#    res1 += (final[key][0])
for key in final2:
    res2 += final2[key][0]

#print(res1)
#print(res2)

