import copy
inp = open("input.txt", 'r').read().splitlines()


print(inp)
inp = [list(line) for line in inp]
print(inp[0][2])


for i, line in enumerate(inp):
    if "S" in line:
        startpos = [i, line.index("S")]
print(startpos)
print(ord('a') -97)

curpos = startpos
prevpos = [-1,-1]
inp[curpos[0]][curpos[1]] = 'a'
steps = 0
tempinp = copy.deepcopy(inp)

while inp[curpos[0]][curpos[1]] != 'E':
    cur = inp[curpos[0]][curpos[1]]
    if prevpos == curpos:
        print(cur, ord(cur))
        print(curpos, prevpos, startpos)
        inp[curpos[0]][curpos[1]] = 'X'
        inp[startpos[0]][startpos[1]] = 'a'
        tempinp = copy.deepcopy(inp)
        st = [' '.join(inp[i]) for i, line in enumerate(inp)]
        print('\n'.join(st))
        print('\n\n\n')
        curpos = startpos
    steps += 1
    try:
        check = ord(tempinp[curpos[0]][curpos[1]+1])
        if check - ord(cur) == 1 or check - ord(cur) == 0 or (chr(check)=='E' and cur == 'z'):
            # tempinp = copy.deepcopy(inp)
            tempinp[curpos[0]][curpos[1]] = 'X'
            prevpos = curpos
            curpos = [curpos[0], curpos[1]+1]
            # print("1")
            continue
    except IndexError:
        pass
    try:
        check = ord(tempinp[curpos[0]+1][curpos[1]])
        if check - ord(cur) == 1 or check - ord(cur) == 0 or (chr(check)=='E' and cur == 'z'):
            # tempinp = copy.deepcopy(inp)
            tempinp[curpos[0]][curpos[1]] = 'X'
            prevpos = curpos
            curpos = [curpos[0]+1, curpos[1]]
            # print("3")
            continue
    except IndexError:
        pass
    try:
        check = ord(tempinp[curpos[0]-1][curpos[1]])
        if check - ord(cur) == 1 or check - ord(cur) == 0 or (chr(check)=='E' and cur == 'z'):
            # tempinp = copy.deepcopy(inp)
            tempinp[curpos[0]][curpos[1]] = 'X'
            prevpos = curpos
            curpos = [curpos[0]-1, curpos[1]]
            # print("4")
            continue
    except IndexError:
        pass
    try:
        check = ord(tempinp[curpos[0]][curpos[1]-1])
        if check - ord(cur) == 1 or check - ord(cur) == 0 or (chr(check)=='E' and cur == 'z'):
            # tempinp = copy.deepcopy(inp)
            tempinp[curpos[0]][curpos[1]] = 'X'
            prevpos = curpos
            curpos = [curpos[0], curpos[1]-1]
            # print("2")
            continue
    except IndexError:
        pass
    prevpos = curpos


print(steps)

#128 too low
