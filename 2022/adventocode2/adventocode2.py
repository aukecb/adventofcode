
def get_input():
    f = open("./adventocode2.txt", "r")
    l = f.read().split("\n")
    print(f.read())

    return l


def part_one(input):
    points = 0
    print(input)
    for play in input[:len(input) -1]:
        match play[2]:
            case "X":
                points += 1
                match play[0]:
                    case "A":
                        points += 3
                    case "B":
                        points += 0 
                    case "C":
                        points += 6
            case "Y":
                points += 2
                match play[0]:
                    case "A":
                        points += 6
                    case "B":
                        points += 3 
                    case "C":
                        points += 0
            case "Z":
                points += 3
                match play[0]:
                    case "A":
                        points += 0
                    case "B":
                        points += 6 
                    case "C":
                        points += 3

    return points

def get_points(result):
    match result:
        case "X":
            return 0
        case "Y":
            return 3
        case "Z":
            return 6
            


def part_two(input):
    points = 0

    for play in input[:len(input)-1]:
        print(play)
        match play[0]:
            case "A":
                match play[2]:
                    case "X":
                        points += 3
                        points += 0
                    case "Y":
                        points += 1
                        points += 3
                    case "Z":
                        points += 2
                        points += 6
            case "B":
                match play[2]:
                    case "X":
                        points += 1
                        points += 0
                    case "Y":
                        points += 2
                        points += 3
                    case "Z":
                        points += 3
                        points += 6
            case "C":
                match play[2]:
                    case "X":
                        points += 2
                        points += 0
                    case "Y":
                        points += 3
                        points += 3
                    case "Z":
                        points += 1
                        points += 6
    return points




def main():
    input = get_input()
    print(part_one(input))
    print(part_two(input))

if __name__ == "__main__":
    main()


#12404 too low
#11602
#12214 too low
