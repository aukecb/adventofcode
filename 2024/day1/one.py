import json

left = []
right = []
with open('input.txt', 'r') as f:
    for line in f:
        left.append(int(line.split()[0]))
        right.append(int(line.split()[1]))

def one():
    left.sort()
    right.sort()
    dists = [abs(left[i] - right[i]) for i in range(len(left))]

    print("1: ", sum(dists))


def two():
    simu = []
    for loc in left:
        simu.append(loc * right.count(loc))
    print("2: ", sum(simu))


one()
two()
