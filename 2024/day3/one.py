with open('input.txt', 'r') as f:
    input = f.read()


def mul(x, y):
    return x * y

def find_muls(indices):
    total = []
    for occ in indices:
        start = input[occ:occ+20].find('(')
        comma = input[occ:occ+20].find(',')
        end = input[occ:occ+20].find(')')
        try:
            total.append(mul(int(input[occ+start+1:occ+comma]), int(input[occ+comma+1:occ+end])))
        except Exception as e:
            pass
    
    # print(total)
    print(sum(total))
    return total

def one():
    sub = "mul("
    res = [i for i in range(len(input)) if input.startswith(sub, i)]
    total = []
    find_muls(res)
    
def two():
    enabled = True
    enable_sub = "do()"
    disable_sub = "don't()"

    sub = "mul("
    res = [i for i in range(len(input)) if input.startswith(sub, i)]
    dos = [i for i in range(len(input)) if input.startswith(enable_sub, i)]
    donts = [i for i in range(len(input)) if input.startswith(disable_sub, i)]
    new_res = []
    for ind in res:
        if enabled:
            if len(donts) == 0:
                enabled = enabled
                new_res.append(ind)
            elif ind > donts[0]:
                enabled = False
                donts.pop(0)
            else:
                new_res.append(ind)

            if len(dos) != 0:
                if ind > dos[0]:
                    dos.pop(0)
        else:
            if len(dos) == 0:
                enabled = enabled
            elif ind > dos[0]:
                enabled = True
                dos.pop(0)
                new_res.append(ind)

            if len(donts) != 0:
                if ind > donts[0]:
                    donts.pop(0)

    enabled = True
    find_muls(new_res)



one()
two()
