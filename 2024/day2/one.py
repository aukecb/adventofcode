rows = []
with open('input.txt', 'r') as f:
    rows = [[int(item) for item in line.split()] for line in f]



# state 0: descreasing, state 1: increasing
def check_safety(cur_num, prev_num, increasing):
    diff = cur_num - prev_num
    if diff > 0:
        increasing = True
    else:
        increasing = False

    if abs(diff) > 3:
        safe = False
    elif abs(diff) == 0:
        safe = False
    else:
        safe = True
    return safe, increasing




def one():
    prev_state = None
    safe_rows = 0
    safe = None

    for row in rows:
        print(row)
        for i in range(len(row)-1):
            safe, cur_state = check_safety(row[i], row[i+1], -1)
            if i == 0:
                prev_state = cur_state

            if cur_state != prev_state:
                safe = False
                break
            if not safe:
                break

            prev_state = cur_state
        if safe:
            safe_rows +=1 

        safe = None
        prev_state = None

    print(safe_rows)


def two():
    safe_rows = 0
    count = 0
    for i in range(len(rows)):
        print(rows[i])
        row = rows[i]
        skip = False
        state = None
        increasing = None
        faults = 0
        safe_list = [1]
        for j in range(len(row)-1):
            if skip and j <=len(row)-1 and faults < 1: 
                faults += 1
                print("SKIPPING")
                count += 1
                safe, increasing = check_safety(row[j-1],row[j+1], increasing)
            else:
                safe, increasing = check_safety(row[j],row[j+1], increasing)


            if count > 100:
                exit(0)
            print(row[j], row[j+1], safe, increasing)
            if not state:
                state = increasing

            if state != increasing and faults < 1:
                skip = True
            if not safe and faults < 1:
                skip = True
            if safe:
                safe_list.append(1)

        print(safe_list)
        if len(safe_list)+1 >= len(row):
            print("SAFEE")
            safe_rows += 1


    print(safe_rows)

print(rows)
# one()
two()



# 823 too high
# 740 too high
# 694
# 710 answer

