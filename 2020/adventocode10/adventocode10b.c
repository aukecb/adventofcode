#include <stdio.h>

int main(){
	FILE *fp;
	fp = fopen("adventocode10.txt", "r");
	int input;
	int inputArray[1000];
	int whileIndex = 0;
	int differenceArray[1000];
	int outputArray[1000][1000];

	while(fscanf(fp, "%d\n", &input) != EOF){
		printf("%d\n", input);
		inputArray[whileIndex] = input;	
		whileIndex++;
	} 
	printf("\n\n");
	int biggestInteger = 0;
	for(int i = 0; i < whileIndex; i++){
		if(inputArray[i] > biggestInteger){
			biggestInteger = inputArray[i];
		}
	}
	inputArray[whileIndex] = biggestInteger + 3;
	whileIndex++;

	int curValue = 0;
	int differenceIndex = 0;
	for(int i = 0; i < whileIndex; i++){

		if(inputArray[i] - curValue == 1){
			outputArray[0][i] = inputArray[i];
			i = -1;
		}
		if(inputArray[i] - curValue == 2){
			outputArray[1][i] = inputArray[i];
			i = -1;
		}
		if(inputArray[i] - curValue == 3){
			outputArray[2][i] = inputArray[i];
			i = -1;
		}
		curValue = inputArray[i];

	}

	for(int i = 0; i < 2; i++){
		for(int j = 0; j < whileIndex; j++){
			printf("%d, ", outputArray[i][j]);
		}
		printf("\n");
	}

	int amountOfOne = 0;
	int amountOfTwo = 0;
	int amountOfThree = 0;
	for(int i = 0; i <= differenceIndex; i++){
		// printf("%d\n", differenceArray[i]);
		switch(differenceArray[i]){
			case 1:
				amountOfOne++;
			break;
			case 2:
				amountOfTwo++;
			break;
			case 3:
				amountOfThree++;
			break;
		}
	}
	printf("\n%d\n%d\n%d\n",amountOfOne, amountOfTwo, amountOfThree );

	printf("\n%d\n", biggestInteger);
	printf("%d\n", amountOfThree * amountOfOne );
}

//answer = 1920