#include <stdio.h>

int main(){
	FILE *fp;
	fp = fopen("adventocode10.txt", "r");
	int input;
	int inputArray[1000];
	int whileIndex = 0;
	int differenceArray[1000];

	while(fscanf(fp, "%d\n", &input) != EOF){
		printf("%d\n", input);
		inputArray[whileIndex] = input;	
		whileIndex++;
	} 
	printf("\n\n");
	int biggestInteger = 0;
	for(int i = 0; i < whileIndex; i++){
		if(inputArray[i] > biggestInteger){
			biggestInteger = inputArray[i];
		}
	}
	inputArray[whileIndex] = biggestInteger + 3;
	whileIndex++;

	int curValue = 0;
	int differenceIndex = 0;
	int checked = 0;
	for(int i = 0; i <= whileIndex; i++){
		if(i == whileIndex  && checked == 0){
			checked = 1;
			i = -1;
		}else if(i == whileIndex  && checked == 1){
			checked = 2;
			i = -1;
		}else if(i == whileIndex  && checked == 2){
			i = -1;
			checked = 3;
		}
		switch(checked){
			case 0:
				if((inputArray[i] - curValue) == 1){
					differenceArray[differenceIndex] = inputArray[i] - curValue;
					differenceIndex++;
					curValue = inputArray[i];
					printf("%d\n", curValue);
					i = -1;		
					checked = 0;	
				}				
			break;
			case 1:
				if((inputArray[i] - curValue) == 2){
					differenceArray[differenceIndex] = inputArray[i] - curValue;
					differenceIndex++;
					curValue = inputArray[i];
					printf("%d\n", curValue);
					i = -1;		
					checked = 0;	
				}
			break;
			case 2:
				if((inputArray[i] - curValue) == 3){
					differenceArray[differenceIndex] = inputArray[i] - curValue;
					differenceIndex++;
					curValue = inputArray[i];
					printf("%d\n", curValue);
					i = -1;		
					checked = 0;	
				}
			break;;
		}
	}

	int amountOfOne = 0;
	int amountOfTwo = 0;
	int amountOfThree = 0;
	for(int i = 0; i <= differenceIndex; i++){
		// printf("%d\n", differenceArray[i]);
		switch(differenceArray[i]){
			case 1:
				amountOfOne++;
			break;
			case 2:
				amountOfTwo++;
			break;
			case 3:
				amountOfThree++;
			break;
		}
	}
	printf("\n%d\n%d\n%d\n",amountOfOne, amountOfTwo, amountOfThree );

	printf("\n%d\n", biggestInteger);
	printf("%d\n", amountOfThree * amountOfOne );
}

//answer = 1920