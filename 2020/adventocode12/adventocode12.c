#include <stdio.h>
#include <string.h>

FILE *fp;

int currentDirection = 1;
char directions[4] = {'N','E','S','W'};
int movements[2] = {0,0}; //pos 0=north/south 	pos 1=east/west
char inputChar;
int inputInt;

int partOne(FILE *fp){
	int movements[2] = {0,0}; //pos 0=north/south 	pos 1=east/west

	while(fscanf(fp, "%c%d\n", &inputChar, &inputInt) != EOF){
		// printf("%d\n", inputInt);
		printf("North/South = %d\t\t East/West = %d\t\t currentDirection = %c\n",movements[0], movements[1], directions[currentDirection]);

		printf("%c", inputChar);
		printf("%d\n", inputInt);
		if(inputChar == 'F'){
			inputChar = directions[currentDirection];
		}
		if(inputChar == 'R'){
			if(inputInt == 90){
				currentDirection +=1;
			}else if(inputInt == 180){
				currentDirection +=2;
			}else if(inputInt == 270){
				currentDirection +=3;
			}
		}else if(inputChar == 'L'){
			if(inputInt == 90){
				currentDirection -=1;
			}else if(inputInt == 180){
				currentDirection -=2;
			}else if(inputInt == 270){
				currentDirection -=3;
			}
		}
		if(currentDirection == 4){
			currentDirection = 0;
		}else if(currentDirection > 4){
			currentDirection = currentDirection - 4;
		}else if(currentDirection < 0){
			currentDirection = currentDirection + 4;
		}


		if(inputChar == 'N'){
			movements[0] += inputInt;
		}else if(inputChar == 'E'){
			movements[1] += inputInt;
		}else if(inputChar == 'S'){
			movements[0] -= inputInt;
		}else if(inputChar == 'W'){
			movements[1] -= inputInt;
		}

	}

	printf("North/South = %d\t East/West = %d\t currentDirection = %c\t\n absoluteDirectionsNorth/South=%d\t absoluteDirectionsEast/West=%d\t sumofabsolutes= %d\n",movements[0], movements[1], directions[currentDirection], abs(movements[0]), abs(movements[1]), abs(movements[0]) + abs(movements[1]));
	return 0;
}

int partTwo(FILE *fp){
	int shipPos[2] = {0,0};
	int waypointPos[2] = {1,10};

	while(fscanf(fp, "%c%d\n", &inputChar, &inputInt) != EOF){
		// printf("%d\n", inputInt);
		printf("SHIP: North/South = %d\t East/West = %d\nWAYPOINT: North/South = %d\t East/West = %d\n",shipPos[0], shipPos[1], waypointPos[0], waypointPos[1]);

		printf("%c", inputChar);
		printf("%d\n", inputInt);
		if(inputChar == 'F'){
			shipPos[0] += waypointPos[0] * inputInt;
			shipPos[1] += waypointPos[1] * inputInt;
		}
		int tempWaypointPos0 = waypointPos[0];
		int tempWaypointPos1 = waypointPos[1];
		if(inputChar == 'R'){
			if(inputInt == 90){
				waypointPos[0] = tempWaypointPos1 * -1;
				waypointPos[1] = tempWaypointPos0;
			}else if(inputInt == 180){
				waypointPos[0] = tempWaypointPos0 * -1;
				waypointPos[1] = tempWaypointPos1 * -1;
			}else if(inputInt == 270){
				waypointPos[0] = tempWaypointPos1;
				waypointPos[1] = tempWaypointPos0 * -1;
			}
		}else if(inputChar == 'L'){
			if(inputInt == 90){
				waypointPos[0] = tempWaypointPos1;
				waypointPos[1] = tempWaypointPos0 * -1;
			}else if(inputInt == 180){
				waypointPos[0] = tempWaypointPos0 * -1;
				waypointPos[1] = tempWaypointPos1 * -1;
			}else if(inputInt == 270){
				waypointPos[0] = tempWaypointPos1 * -1;
				waypointPos[1] = tempWaypointPos0;
			}
		}


		if(inputChar == 'N'){
			waypointPos[0] += inputInt;
		}else if(inputChar == 'E'){
			waypointPos[1] += inputInt;
		}else if(inputChar == 'S'){
			waypointPos[0] -= inputInt;
		}else if(inputChar == 'W'){
			waypointPos[1] -= inputInt;
		}

	}

	printf("North/South = %d\t East/West = %d\t currentDirection = %c\t\n absoluteDirectionsNorth/South=%d\t absoluteDirectionsEast/West=%d\t sumofabsolutes= %d\n",shipPos[0], shipPos[1], directions[currentDirection], abs(shipPos[0]), abs(shipPos[1]), abs(shipPos[0]) + abs(shipPos[1]));
	return 0;	
}





int main(){	
	fp = fopen("adventocode12.txt", "r");
	// partOne(fp);
	partTwo(fp);
}

//tried 2994
//tried 232878
