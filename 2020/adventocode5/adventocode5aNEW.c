#include <stdio.h>

int main(){
	FILE *fp;
	fp = fopen("adventocode5.txt", "r");
	int rows = 760;
	int columns = 500;
	char inputChar[rows][columns];

	for(int i = 0; i <= rows; i++){
		fscanf(fp, "%s", inputChar[i]);
	}

	for(int i = 0; i <= rows; i++){
		printf(inputChar[i]);
	}
}