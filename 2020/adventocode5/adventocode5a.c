#include <stdio.h>

int main(){
	FILE *fp;                                         
	fp = fopen("adventocode5.txt","r");
	int rows = 760; //760
	int columns = 500;
	char inputChar[rows][columns];
	int frontBack[rows][10 -3];
	int rangeInt = 128;
	int lower = 0;
	int higher = 127;
	int answer[rows][3];
	int defAnswer = 0;

	for(int i =0; i<=rows; i++){
		fscanf(fp,"%s",inputChar[i]);
	}

	// printf("%c",inputChar[759][8]);

	for(int i = 0; i<= rows; i++){
		lower = 0;
		higher = 127;
		rangeInt = 128;
		for(int j = 0; j <=6; j++){
			rangeInt /= 2;
			if(inputChar[i][j] == 'F'){
				higher -= rangeInt;

			}else if(inputChar[i][j] == 'B'){
				lower += rangeInt;
			}
			printf("lower: %d higher: %d\n",lower, higher);
		}
		answer[i][0] =  higher;

		rangeInt = 8;
		lower = 0;
		higher = 7;
		for(int j = 7; j <= 9; j++){
			rangeInt /=2;
			if(inputChar[i][j] == 'R'){
				lower += rangeInt;				
			}else if(inputChar[i][j] == 'L'){
				higher -= rangeInt;
			}
			printf("lower: %d higher: %d\n",lower, higher);
		}
		answer[i][1] = higher;

		answer[i][2] = (answer[i][0] * 8) + answer[i][1];
		printf("(%d * 8) + %d = %d", answer[i][0], answer[i][1],answer[i][2]);
		// printf("%s\n", inputChar[i]);	
		
	}

	for(int i = 0; i<= rows; i++){
		if(defAnswer < answer[i][2]){
			defAnswer = answer[i][2];
			higher = i;
		}
	}
	printf("%d %d", defAnswer, higher);

	
	return 0;
}