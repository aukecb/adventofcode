#include <stdio.h>
#include <string.h>

char types[][4] = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

int checkComplete(char* text){
	for (int i = 0; i < 7; i++){
		if (strstr(text, types[i]) == NULL){
			return 0;
		}
	}
	return 1;
}

int main(){
	FILE *fp;
	fp = fopen("adventocode4.txt","r");
	int rows = 760;
	int columns = 500;
	char inputChar[rows][columns];
	char text[90000];
	char text2[800];
	char prevText;
	char string[800];
	int count = 0;

	while(fgets(string, sizeof(string),fp)){

		// printf(string);
		if(string[0] == '\n'){
			// if(strstr(text, "byr") != NULL && strstr(text, "iyr") != NULL && strstr(text, "eyr") != NULL && strstr(text, "hgt") != NULL && strstr(text, "hcl") != NULL && strstr(text, "ecl") != NULL && strstr(text, "pid") != NULL /*|| strstr(text, "cid") != NULL*/){
			// 	if(strstr(text, "cid") != NULL){
			// 		count++;
			// 	}else{
			// 		count++;
			// 	}
			// 	// printf("------------Detected valid-----------\n");
			// 	// printf(text);
			// 	// printf("-----------------------\n\n");
			// }else{
			// 	printf("-----DETECTED invalid---------\n");
			// 	printf(text);
			// 	printf("------------------------\n");
			// }
			if(checkComplete(text) == 1){
				count++;
			}
			memset(text, 0, sizeof(text));
		}else{
			strcat(text, string);
		}
	}
	printf("%d", count);
}

//guessed 20,41,240,241,239,228,182,229
//maybe 124 , 105, 230
// 230 is right