#include <stdio.h>
#include <stdbool.h>
#include <string.h>

FILE *fp;
char inputArray[1000];
char string[1000];
char prevString[1000];
char groupAnswer[1000][1000];
int groupIndex = 0;
int charIndex = 0;

/*
a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z
49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74
0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
*/
int checkGroupAnswers(char* answer){
	int checkedLetters[25] = {0};
	checkedLetters[25] = 0;
	// printf("%d\n", strlen(answer));
	for(int i =0; i <= strlen(answer); i++){
		int answerInt = (answer[i] - '0') - 49;
		checkedLetters[answerInt] = 1;
	}
	int returnVal = 0;

	for(int i = 0; i <= 25; i++){
		if(checkedLetters[i] == 1){
			// printf("%c",(checkedLetters[i] + '0') + 49);
			returnVal++;
		}
	}
	// printf("returnVal: %d\n", returnVal);
	return returnVal;
}

int checkGroupAnswers2(char* answer){
	int checkedLetters[25] = {0};
	checkedLetters[25] = 0;
	char personAnswer[1000][1000] = {0};
	int personIndex = 0;
	charIndex = 0;
	// printf("%s", answer);
	for(int i = 0; i <= strlen(answer); i++){
		personAnswer[personIndex][charIndex] = answer[i];
		charIndex++;
		if(answer[i] == '\n'){
			charIndex = 0;
			personIndex++;
		}
	}
	personIndex = personIndex - 1;

	for(int i = 0; i <= personIndex;i++){
		for(int j = 0; j <= strlen(answer); j++){
			int answerInt = (personAnswer[i][j] - '0') - 49;
			checkedLetters[answerInt] += 1;
		}
	}
	int returnVal = 0;
	for(int i = 0; i <= 25; i++){
		if(checkedLetters[i] == personIndex){
			returnVal++;
			// printf("%c\n",(i +'0') + 49);
		}
	}
	// printf("PersonIndex:%d\t returnVal:%d\n", personIndex, returnVal);

	// printf("person1: %s\t person2: %s\n", personAnswer[0], personAnswer[1]);

	return returnVal;
}

int main(){
	fp = fopen("adventocode6.txt", "r");
	while(fscanf(fp, "%c",string) != EOF){
		printf("%s", string);
		groupAnswer[groupIndex][charIndex] = *string;
		charIndex++;
		if(string[0] == '\n' && prevString[0] == '\n'){
			groupIndex++;
			charIndex = 0;
			// printf("%s", string);
			// printf("JOEJOEJOEJOE\n");
		}
		prevString[0] = string[0];
	}

	int finalAnswer = 0;
	for(int i = 0; i <= groupIndex; i++){
		finalAnswer += checkGroupAnswers(groupAnswer[i]);
	}

	printf("\nNumber of groups: %d", groupIndex);
	printf("\n");
	printf("finalAnswer1: %d\n", finalAnswer);
	finalAnswer = 0;
	for(int i = 0; i < groupIndex; i++){
		finalAnswer += checkGroupAnswers2(groupAnswer[i]);
	}
	printf("FinalAnswer2: %d\n", finalAnswer);
	// printf("\n%d\n",checkGroupAnswers("abcdefghijklmnopqrstuvwxyz"));
	// printf("%s", groupAnswer[2]);
	// for(int i = 0; i <= groupIndex; i++){
	// 	printf("groupanswer: %s", groupAnswer[i]);
	// }
}

//guessed 6843
//guessed 7341
//right answer 7128
//guessed 2 2536
//right answer 2 3640