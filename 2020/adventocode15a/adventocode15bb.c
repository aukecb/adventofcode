#include <stdio.h>	
#include <stdbool.h>


#define size 30000001
int noArray[size];
int hasNumber[size];
int calcArray[2] = {-1,-1};
int inputNumbers = 0;
int noArrayIndex = 0;
int numbersFound = 0;
bool foundNumber = false;
int newNumber = 0;



int main(){
	printf("Enter amount of input numbers: ");
	scanf("%d", &inputNumbers);
	for(int i = 0; i < size; i++){
		hasNumber[i] = -1;
	}
	for(int i = 0; i < inputNumbers; i++){
		printf("Enter inputNumber %d: ", i);
		scanf("%d", &noArray[i + 1]);
		noArrayIndex++;
	}

	for(int i = 1; i <= inputNumbers -1; i++){
		hasNumber[noArray[i]] = i;
	}

	// for(int i = 0; i < size; i++){
	// 	printf("%d: %d\n", i, hasNumber[i]);
	// }

	for(int i = 1; i < size; i++){
		// printf("%d", noArrayIndex);
		// printf("curNumber: %d\t hasNumber: %d\n", noArray[noArrayIndex],hasNumber[noArray[noArrayIndex -1]]);
		if(hasNumber[noArray[noArrayIndex]] != -1){
			calcArray[0] = noArrayIndex;
			calcArray[1] = hasNumber[noArray[noArrayIndex]];
			newNumber = calcArray[0] - calcArray[1];
			// printf("calcArray[0] = %d\t calcArray[1] = %d\t newNumber = %d\n",calcArray[0], calcArray[1], newNumber);
		}else if(hasNumber[noArray[noArrayIndex]] == -1){
			newNumber = 0;
		}
		noArrayIndex++;
		noArray[noArrayIndex] = newNumber;
		hasNumber[noArray[noArrayIndex -1]] = noArrayIndex -1 ;
		// printf("%d\n", noArray[i]);
	}
	printf("\n\n");
	for(int i = 1; i < size; i++){
		printf("%d: %d\n", i, noArray[i]);
	}
}