#include <stdio.h>	
#include <stdbool.h>

int noArray[3000];
int calcArray[2] = {-1,-1};
int inputNumbers = 0;
int noArrayIndex = 0;
int numbersFound = 0;
bool foundNumber = false;



int main(){
	printf("Enter amount of input numbers: ");
	scanf("%d", &inputNumbers);
	for(int i = 0; i < inputNumbers; i++){
		printf("Enter inputNumber %d: ", i);
		scanf("%d", &noArray[i + 1]);
		noArrayIndex++;
	}
	// printf("%d, %d, %d",noArray[0],noArray[1],noArray[2]);

	for(int i = 1; i < 3000; i++){
		// printf("%d", noArrayIndex);
		for(int j = noArrayIndex; j > 0; j--){
			// printf("noArray[i] = %d\tnoArray[j] = %d\n",noArray[i], noArray[j]);
			if(noArray[noArrayIndex] == noArray[j]){
				foundNumber = true;
				if(numbersFound == 0){
					calcArray[0] = j;
					numbersFound++;
				}else if(numbersFound == 1 && j != calcArray[0]){
					calcArray[1] = j;
					numbersFound++;
				}else if(numbersFound == 2){
					int newNumber = calcArray[0] - calcArray[1];
					printf("calcArray[0] = %d\t calcArray[1] = %d\t newNumber = %d\n",calcArray[0], calcArray[1], newNumber);
					noArrayIndex++;
					noArray[noArrayIndex] = newNumber;
					numbersFound = 0;
					calcArray[0] = -1;
					calcArray[1] = -1;
					break;
				}else{
					calcArray[0] = -1;
					calcArray[1] = -1;
					numbersFound = 0;
					foundNumber = false;
				}
			}
		}
		if(!foundNumber){
			numbersFound = 0;
			int newNumber = 0;
			noArrayIndex++;
			noArray[noArrayIndex] = newNumber;
		}
		foundNumber = false;
		printf("%d\n", noArray[i]);
	}
	printf("\n\n");
	for(int i = 1; i < 3000; i++){
		printf("%d: %d\n", i, noArray[i]);
	}

	printf("2020:%d\t2021:%d",noArray[2020],noArray[2021]);
}