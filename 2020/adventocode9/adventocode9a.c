#include <stdio.h>
#include <stdbool.h>

int main(){
	// char* input;
	FILE *fp;
	fp = fopen("adventocode9.txt", "r");
	int index = 0;
	int whileIndex = 0;
	int input;
	int inputArray[1000];
	bool isOkay = false;
	int amountToCheck = 25;

	int lastSet[amountToCheck];

	for(int i = 0; i < amountToCheck; i++){
		lastSet[i] = 0;
	}

	while(fscanf(fp ,"%d\n", &input) != EOF){
		// printf("%d\n",input[whileIndex]);
		// printf("%d\n", input);
		// inputArray[whileIndex] = input;
		for(int i =0; i < amountToCheck; i++){
			for(int j = i+1; j < amountToCheck; j++){
				if(input == lastSet[i] + lastSet[j]){
					// printf("OK\n");
					isOkay = true;
					break;
				}else{
					isOkay = false;
				}
			}
			if(isOkay){
				break;
			}
		}
		if(!isOkay){
			printf("{");
			for(int i = 0; i < amountToCheck; i++){
				printf("%d, ", lastSet[i]);
			}
			printf("}\n");
			printf("%d: FAULT: %d\n",whileIndex, input);
		}
		int tempInt[amountToCheck + 1];
		for(int i = 0; i < amountToCheck; i++){
			tempInt[i + 1] = lastSet[i];
		}
		tempInt[0] = input;
		for(int i = 0 ; i < amountToCheck; i++){
			lastSet[i] = tempInt[i];
		}

		whileIndex++;
	}
	return 0;
}

//tried: 53
//right answer = 393911906