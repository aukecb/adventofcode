#include <stdio.h>
#include <string.h>

// #define rowNo 98
// #define columnNo 93

#define rowNo 10
#define columnNo 10

FILE *fp;

char charArray[rowNo][columnNo] = {0};
char tempCharArray[rowNo][columnNo] = {0};
char string;

int checkAdjacentSeats(int row, int column){
	int occupiedCount = 0;
	for(int i = -1; i <=1; i++){
		for(int j = -1; j <=1 ; j++){
			int calculatedRow = row + i;
			int calculatedColumn = column + j;
			if(!(calculatedRow == row && calculatedColumn == column)){
				if(charArray[calculatedRow][calculatedColumn] == '#'){
					occupiedCount++;
				}
			printf("Checking: charArray[%d][%d] = %c\n",row + i, column + j, charArray[row + i][column + j]);
			}
		}
	}
	printf("occupiedCount: %d\n", occupiedCount);
	return occupiedCount;
}


int main(){
	fp = fopen("adventocode11test.txt", "r");
	int columnCount = 0;
	int rowCount = 0;
	while(fscanf(fp, "%c", &string) != EOF){
		printf("%c",string);
		charArray[rowCount][columnCount] = string;
		columnCount++;
		if(string == '\n'){
			rowCount++;
			columnCount = 0;
		}
	}

	printf("\nrows: %d\t columns: %d\n", rowCount, columnCount);
	for(int i = 0; i <=rowNo; i++){
		for(int j = 0; j < columnNo; j++){
			printf("%c", charArray[i][j]);
			if(charArray[i][j] == 'L' && checkAdjacentSeats(i, j) == 0){
				tempCharArray[i][j] = '#';
			}else if(charArray[i][j] == '#' && checkAdjacentSeats(i, j) >= 4){
				tempCharArray[i][j] = 'L';
			}else{
				tempCharArray[i][j] = charArray[i][j];
			}
		}
		printf("\n");
	}
	printf("\n");
	for(int i = 0; i <= rowNo; i++){
		for(int j = 0; j < columnNo; j++){
			charArray[i][j] = tempCharArray[i][j];
			printf("%c", tempCharArray[i][j]);
		}
	printf(	"\n");
	}

}