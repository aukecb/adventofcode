#include <stdio.h>
#include <string.h>

int main(){
	FILE *fp;
	fp = fopen("adventocode8.txt", "r");
	int rows = 654;
	char inputString[rows][255];
	char minPlusChar[rows];
	int integerChar[rows];
	int accumulator = 0;
	int j = 0;
	int indexArray[rows];
	int counter = 0;

	for(int i = 0; i <= rows; i++){
		// fgets(inputString,rows,fp);
		fscanf(fp, "%s %[+,-]%d",&inputString[i], &minPlusChar[i], &integerChar[i]);
		
	}

	for(int i = 0; i <= rows; i++){
		printf("%s %c%d\n", inputString[i], minPlusChar[i], integerChar[i]);
		if(strcmp(inputString[i], "jmp") == 0){
			if(minPlusChar[i] == '+'){
				i = (i + integerChar[i]) - 1;
			}else if(minPlusChar[i] == '-'){
				i = (i - integerChar[i]) -1;
			}
			printf("i = %d", i);
			indexArray[j] = i;
			j++;
		}else if(strcmp(inputString[i], "acc") == 0){
			if(minPlusChar[i] == '+'){
				accumulator = accumulator + integerChar[i];
				// printf("accumulator++");
			}else if(minPlusChar[i] == '-'){
				accumulator = accumulator - integerChar[i];
				// printf("accumulator--");
			}
		}
		for(int f=0; f<=j; f++){
			for(int g = f +1; g<=j; g++){
				if(indexArray[f] == indexArray[g]){
					printf("indexArray:%d %d\n", f, g);
					printf("--------------------------------accumulator: %d", accumulator);	
					return 0;
				}
				// printf("%d ",indexArray[g]);
			}
			// printf("%d ",indexArray[f] );
		}
		// printf("%d\n", accumulator);
	}
	
	return 0;
}

//guessed 82, -3971, 320, 339, -9