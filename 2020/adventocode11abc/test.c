#include <stdio.h>

int * getArray(int * returnArray, int a, int b, int c){
	returnArray[0] = a;
	returnArray[1] = b;
	returnArray[2] = c;

	return returnArray;
}

int main(){

	int array[3];
	int* ptr = getArray(array,4,5,6);

	printf("%d\n", ptr[0]);

	for(int i = 0; i < 3; i++){
		printf("%d: %d\n",i, array[i]);
	}
	return 0;
}