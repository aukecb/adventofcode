#include <stdio.h>

// #define x 0
#define arraySize 9

// int busID[10] = {13,41,37,419,19,23,29,421,17};
// int busID[arraySize] = {13,-1,-1,41,-1,-1,-1,37,-1,-1,-1,-1,-1,419,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,19,-1,-1,-1,23,-1,-1,-1,-1,-1,29,-1,421,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,17};
int busID[arraySize] = {7,13,-1,-1,59,-1,31,19};
int arrayFilledSize = 0;
int curTime;
// int plannedTime = 1002394;
int plannedTime = 1068735;

int checkPartOne(){
	int closestBusses[arraySize] = {0};
	int leastTimeDiff = 100;
	while(curTime <= plannedTime + 50){
		curTime++;
		for(int i = 0; i < arraySize; i++){
			if(busID[i] > 0){
				if(curTime > plannedTime - 200){
					if(curTime % busID[i] == 0 ){
						printf("Curtime: %d\tBus:%d\n",curTime, busID[i]);
						if(curTime > plannedTime -1 && curTime < plannedTime +50){
							closestBusses[(curTime - plannedTime)] = busID[i];
							if(leastTimeDiff > curTime - plannedTime){
								leastTimeDiff = curTime - plannedTime;
							}
						}
					}
				}
			}
		}
	}
	for(int i = 0; i < arraySize; i++){
		if(closestBusses[i] != 0){
			printf("BusID: %d\tTimeDifference: %d\tTimeDifference * BusID: %d\n", closestBusses[i], i, closestBusses[i] * i);
		}
	}
	printf("FINAL: %d * %d = %d\n", closestBusses[leastTimeDiff], leastTimeDiff, leastTimeDiff * closestBusses[leastTimeDiff]);
}

int checkPartTwo(){
	int answerCount = 0;
	int completed = 0;
	while(!completed){
		for(int i = 0; i < arraySize; i++){
			if(busID[i] == -1){
				answerCount++;
			}else if(busID[i] > 0){
				if(curTime % busID[i] == 0){
					answerCount++;
					completed =1;
					// printf("Curtime: %d\t busID: %d\t Index: %d\n", curTime, busID[i], i);
					printf("Curtime: %d\tanswerCount: %d\n", curTime, answerCount);
				}
			}else{
				 answerCount = 0;
			}
		}
			if(answerCount == arrayFilledSize || answerCount == arrayFilledSize -1){
				printf("Curtime: %d\t\n", curTime);
				completed = 1;
			}
		curTime++;
	}

}

int main(){
	int biggstID = 0;
	int smallestID = 0;
	for(int i = 0; i < arraySize;i++){
		if(busID[i] > biggstID){
			biggstID = i;
		}else if(busID[i] < smallestID){
			smallestID = i;
		}
	}
	printf("%d\n", biggstID);
	arrayFilledSize = sizeof(busID)/ sizeof(busID[0]);
	printf("SIZE: %d\n", sizeof(busID)/ sizeof(busID[0]));
	// checkPartOne();
	checkPartTwo();


	return 0;
}

//answer1: 2947