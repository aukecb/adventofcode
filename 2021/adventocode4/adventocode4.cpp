#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#ifdef __linux__
std::string path = "../adventocode4/adventocode4.txt";
#elif _WIN32
    std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\adventocode4\\adventocode4.txt";
#endif

typedef std::vector<std::vector<std::pair<bool, std::string>>> VVP;
typedef std::vector<VVP> VVVP;

std::string fileString;
std::ifstream file;
std::vector<std::string> numbers;
int curNumberIndex = 0;

void printBoard(VVP &board){
    for(int j = 0; j < board.size(); j++){
        for(int k = 0; k < board[j].size(); k++){
            std::cout << '(' << board[j][k].second << ',' << board[j][k].first << ") ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void printBoards(VVVP &boards){
    for(int i = 0; i < boards.size(); i++){
        printBoard(boards[i]);
        std::cout << std::endl;
    }
}

VVVP getBoards(){
    VVVP returnVector = {};
    std::string direction;
    file.open(path);
    std::getline(file, fileString);
    size_t pos = 0;
    while((pos = fileString.find(',')) != std::string::npos){
        numbers.push_back(fileString.substr(0, pos));
        fileString.erase(0, pos + 1);
    }
    std::vector<std::vector<std::pair<bool, std::string>>> board = {};
    std::vector<std::pair<bool, std::string>> row;
    auto count =0;
    while(std::getline(file, fileString)){
        row = {};
        if(!fileString.empty()){
            if(board.size() <= 4){
                for(int i = 0; i <= fileString.length() -2; i+= 3){
                    if(fileString[i] == ' '){
                        row.push_back(std::make_pair(0, fileString.substr(i + 1, 1)));
                    }else{
                        row.push_back(std::make_pair(0, fileString.substr(i ,2)));
                    }
                }
                board.push_back(row);
            }
            if(board.size() == 5){
                returnVector.push_back(board);
                board = {};
            }
        }
    }
    printBoards(returnVector);

    file.close();
    return returnVector;
}

VVP checkBoard(VVP &board, std::string &number){
    for(int i = 0; i < board.size(); i++){
        auto countHorizontal = 0, countVertical = 0;
        for(int j = 0; j < board[i].size(); j++){
            if(board[i][j].second == number){
                board[i][j].first = true;
//                std::cout << board[i][j].second << std::endl;
            }
            if(board[i][j].first){
                countHorizontal++;
            }
            if(board[j][i].first){
                countVertical++;
            }
        }
        if(countHorizontal == 5 || countVertical == 5){
            std::cout << "WINNER" << board[i][0].second << std::endl;
            return board;

        }else{
            countHorizontal = 0;
            countVertical = 0;
        }
    }
    VVP returnVVP = {};
    if(!returnVVP.empty()){
        printBoard(returnVVP);
    }
    return returnVVP;
}

VVP checkBoard2(VVP &board, std::string &number){
    for(int row = 0; row < board.size(); row++){
        auto countHorizontal = 0, countVertical = 0;
        for(int col = 0; col < board[row].size(); col++){
            if(board[row][col].second == number){
                board[row][col].first = true;
//                std::cout << board[row][col].second;
            }

            if(board[row][col].first){
                countHorizontal++;
            }
            if(board[col][row].first){
                countVertical++;
            }

        }
        if(countHorizontal == 5 || countVertical == 5){
            std::cout << "WINNER" << board[row][0].second << std::endl;
            return board;

        }else{
            countHorizontal = 0;
            countVertical = 0;
        }
    }
    VVP returnVVP = {};
    if(!returnVVP.empty()){
        printBoard(returnVVP);
    }
    return returnVVP;
}

int partOne(VVVP &input){
    VVP winningBoard;
    auto win = 0;
    auto lastNumber = 0;
    for(auto number : numbers){
        std::cout << "CURRENTNUMBER: "  << number << std::endl;
        for(auto &board: input){
            if(!checkBoard2(board, number).empty()){
                winningBoard = board;
                lastNumber = std::stoi(number);
                win = true;
            }
        }
        if(win){
            break;
        }
    }
    printBoard(winningBoard);
    int winningSum = 0;
    for(auto &row : winningBoard){
        for(auto &elem : row){
            if(!elem.first){
                winningSum += std::stoi(elem.second);
            }
        }
    }
    std::cout << "winnningSUM: "<< winningSum << std::endl;
    auto winningProduct = winningSum * lastNumber;
    std::cout << "WinningProduct: "<< winningProduct<< std::endl;
    return winningProduct;
}

int partTwo(VVVP &input){
    VVP winningBoard;
    auto win = 0;
    auto lastNumber = 0;
    printBoards(input);
    for(auto number : numbers){
        std::cout << "CURRENTNUMBER: "  << number << std::endl;
        for(int j =0; j < input.size(); j++){
            if(!checkBoard2(input[j], number).empty()){
                winningBoard = input[j];
                printBoard(input[j]);
//                printBoards(input);
                input.erase(input.begin() + j);
                j = -1;
//                std::cout << "INPUTSIZE : "<< input.size() << "  JJJJ: "<< j <<std::endl;
            }
            if(input.empty()){
                lastNumber = std::stoi(number);
                win = true;
            }
        }
        if(win){
            break;
        }
    }
    printBoard(winningBoard);
    int winningSum = 0;
    for(auto &row : winningBoard){
        for(auto &elem : row){
            if(!elem.first){
                winningSum += std::stoi(elem.second);
            }
        }
    }
    std::cout << "winnningSUM: "<< winningSum << std::endl;
    auto winningProduct = winningSum * lastNumber;
    std::cout << "WinningProduct: "<< winningProduct<< std::endl;
    return winningProduct;
}

int main(){
    std::cout << "helloo world\n";
    auto vector = getBoards();
    for(auto number : numbers){
        std::cout << number << " | ";
    }
    std::cout << "PartOne: " << partOne(vector) << std::endl;
    std::cout << "PartTwo: " << partTwo(vector) << std::endl;
	return 0;
}