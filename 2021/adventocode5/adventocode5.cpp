#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

#ifdef __linux__
std::string path = "../adventocode5/adventocode5.txt";
#elif _WIN32
std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\adventocode5\\adventocode5.txt";
#endif
typedef std::vector<std::vector<int>> VV;
typedef std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> VPP;
//typedef std::vector<VPP> VVPP;

std::string fileString;
std::ifstream file;

int boardSize = 1000;

VPP getLines(){
    VPP returnVector = {};
    file.open((path));
    while(getline(file, fileString)){
        auto x1 = std::stoi(fileString.substr(0, fileString.find(',')));
        fileString.erase(0, fileString.find(',') + 1);
        auto y1 = std::stoi(fileString.substr(0, fileString.find(' ')));
        fileString.erase(0, fileString.find(' ') + 4);
        auto x2 = std::stoi(fileString.substr(0, fileString.find(',')));
        fileString.erase(0, fileString.find(',') + 1);
        auto y2 = std::stoi(fileString.substr(0, fileString.length()));
        fileString.erase(0, fileString.length());
        returnVector.push_back(std::make_pair(std::make_pair(x1, y1), std::make_pair(x2, y2)));
    }
    for(auto &elem : returnVector){
        std::cout << "((" << elem.first.first << ',' << elem.first.second << ")(" << elem.second.first << ',' << elem.second.second << "))" << std::endl;
    }
    file.close();

    return returnVector;
}

VV getEmptyBoard(int size){
    VV board = {};
    for(int i = 0; i < size; i++){
        std::vector<int> temp = {};
        for(int j = 0; j < size; j++){
            temp.push_back(0);
        }
        board.push_back(temp);
    }
    return board;
}

VV updateBoard(VV &board, VPP &lines){
    for(auto &line : lines){
        auto x1 = line.first.first;
        auto y1 = line.first.second;
        auto x2 = line.second.first;
        auto y2 = line.second.second;

        if(x1 == x2){
            for(int i = 0; i <= abs(y2 - y1); i++){
                board[i + std::min(y2, y1)][x1] += 1;
            }
        }else if(y1 == y2){
            for(int i = 0; i <= abs(x2 - x1); i++){
                board[y2][i + std::min(x1, x2)] += 1;
            }
        }
    }
    return board;
}

VV updateBoard2(VV &board, VPP &lines){
    for(auto &line : lines){
        auto x1 = line.first.first;
        auto y1 = line.first.second;
        auto x2 = line.second.first;
        auto y2 = line.second.second;

        if(x1 == x2){
            for(int i = 0; i <= abs(y2 - y1); i++){
                board[i + std::min(y2, y1)][x1] += 1;
            }
        }else if(y1 == y2){
            for(int i = 0; i <= abs(x2 - x1); i++){
                board[y2][i + std::min(x1, x2)] += 1;
            }
        }else{
            auto dist = sqrt((y2 - y1) *(y2 - y1)) + ((x2 - x1) * (x2 - x2));
            std::cout << "((" << x1 << ',' << y1 << ")(" << x2 << ',' << y2 << "))" << " DIST: " << dist << std::endl;
            for(int i = 0; i <= dist; i++){
                if(y1 > y2){
                    if(x1 > x2){
                        board[y1 - i][x1 - i] += 1; //correct
                    }else if (x1 < x2){
                        board[y1 - i][x1 + i] += 1; //correct
                    }
                }else if(y1 < y2){
                    if(x1 > x2){
                        board[y1 + i][x1 - i] +=1;
                    }else if(x1 < x2){
                        board[y1 + i][x1 + i] +=1; // correct
                    }
                }
            }
        }
    }
    return board;
}

int partOne(VV board){
    int count = 0;

    for(int i = 0; i < board.size(); i++){
        for(int j = 0; j < board[0].size(); j++){
            std::cout << board[i][j] << ' ';
            if(board[i][j] > 1){
                count++;
            }
        }
        std::cout << std::endl;
    }
    return count;
}

int partTwo(VV board){
    int count = 0;
    std::cout << "  0 1 2 3 4 5 6 7 8 9" << std::endl;
    std::cout << "  ___________________" << std::endl;
    for(int i = 0; i < board.size(); i++){
        std::cout << i << '|';
        for(int j = 0; j < board[0].size(); j++){
            std::cout << board[i][j] << ' ';
            if(board[i][j] > 1){
                count++;
            }
        }
        std::cout << std::endl;
    }
    return count;
}

int main(){
    auto input = getLines();
    auto emptyBoard = getEmptyBoard(10);
//    auto answer1 = partOne(updateBoard(emptyBoard, input));
//    std::cout << "PartOne: " << answer1 << std::endl;
    auto answer2 = partTwo(updateBoard2(emptyBoard, input));
    std::cout << "PartTwo: " << answer2 << std::endl;
    return 0;
}

//guessed 4825