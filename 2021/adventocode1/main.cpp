#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\";
std::string fileString;
std::ifstream file(path + "adventocode1\\adventocode1.txt");

std::vector<int> getNumbers(){
    std::vector<int> returnVector;
    while(getline(file, fileString)){
        returnVector.push_back(std::stoi(fileString));
    }
    return returnVector;
}

int partOne(std::vector<int> numbers){
    getline(file, fileString);
    int prevNumber = numbers[0];
    int count = 0;
    for(int i = 0; i < numbers.size(); i++){
        if(numbers[i] > prevNumber){
            count++;
        }
        prevNumber = numbers[i];
    }
    return count;
}

int partTwo(std::vector<int> numbers){
    int returnInt;
    std::vector<int> addedVector;
//    std::cout << numbers.size();
    for(int i = 0; i < numbers.size() -2; i++){
//        std::cout << numbers[i + 0] + numbers[i + 1] + numbers[i + 2];
        addedVector.push_back(numbers[i + 0] + numbers[i + 1] + numbers[i + 2]);
//        std::cout << addedVector[i] << " || ";
    }
    return partOne(addedVector);
}

int main(){

    std::vector<int> numberVector = getNumbers();
    std::cout << "Number of increments: " << partOne(numberVector) << std::endl;
    std::cout << "PartTow: " << partTwo(numberVector) << std::endl;
    return 0;
}
