#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#ifdef __linux__
std::string path = "../adventocode9/adventocode9.txt";
#elif _WIN32
std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\adventocode9\\adventocode9.txt";
#endif

using namespace std;

typedef std::vector<std::vector<int>> VV;
typedef std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> VPP;
//typedef std::vector<VPP> VVPP;

std::string fileString;
std::ifstream file;

VV getInput(){
    VV returnVector = {};
    file.open((path));

    while(getline(file, fileString)){
        vector<int> lineVec = {};
        if(returnVector.empty()){
            for(int i = 0; i < fileString.length() + 2; i++){
                lineVec.push_back(10);
            }
            returnVector.push_back(lineVec);
            lineVec = {};
        }
        lineVec.push_back(10);
        for(auto &number : fileString){
            lineVec.push_back(number - '0');
        }
        lineVec.push_back(10);
        returnVector.push_back(lineVec);
    }
    vector<int> lineVec = {};
    for(int i =0; i < fileString.length() + 2; i++){
        lineVec.push_back(10);
    }
    returnVector.push_back(lineVec);
    file.close();

    return returnVector;
}



vector<int> getLowPoints(VV &input){
    vector<int> lowPoints = {};
    for(int i = 1; i < input.size() -1; i++){
        for(int j= 1; j < input[0].size() - 1; j++){
            auto cur = input[i][j];
            if(cur < input[i - 1][j] && cur < input[i][j -1] && cur < input[i +1][j] && cur < input[i][j + 1]){
//                cout << "PUSHING BACK: " << cur << endl;
                lowPoints.push_back(cur);
            }
        }
    }
    return lowPoints;
}

vector<pair<int, int>> getLowIndex(VV &input){
    vector<pair<int, int>> lowPoints = {};
    for(int i = 1; i < input.size() -1; i++){
        for(int j= 1; j < input[0].size() - 1; j++){
            auto cur = input[i][j];
            if(cur < input[i - 1][j] && cur < input[i][j -1] && cur < input[i +1][j] && cur < input[i][j + 1]){
//                cout << "PUSHING BACK: " << cur << endl;
                lowPoints.push_back(make_pair(i, j));
            }
        }
    }
    return lowPoints;
}

int partOne(VV &input){
    auto lowPoints = getLowPoints(input);
    auto count = 0;
    for(auto &n : lowPoints){
        count += n + 1;
    }
    return count;
}

int partTwo(VV &input){
    auto lowPoints = getLowPoints(input);
    vector<float> finalBasin = {};
    vector<float> tempBasin = {};
    for(int i = 1; i < input.size() - 1; i++){
        for(int j = 1; j < input[i].size() -1; j++){
            auto cur = input[i][j];
            auto left = input[i][j - 1];
            auto right = input[i][j + 1];
            auto up = input[i - 1][j];
            auto down = input[i + 1][j];

            while(cur != 9){
                if(cur !=9){
                    tempBasin.push_back(cur);
                }
                if(cur < input[i - 1][j] && cur < input[i][j -1] && cur < input[i +1][j] && cur < input[i][j + 1]) {
                }
            }
        }
    }

    return 0;
}


int main(){
    auto input = getInput();
//    for(int i = 1; i < input.size() -1; i++){
//        for(int j = 1; j < input[i].size() - 1; j++){
//            cout << input[i][j];
//        }
//        cout << endl;
//    }
    auto one = partOne(input);
    cout << "Partone: " << one << endl;
    auto lowIndex = getLowIndex(input);
    return 0;
}