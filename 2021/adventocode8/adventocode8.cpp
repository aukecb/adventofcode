#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <typeinfo>
#include <unordered_set>
#include <algorithm>

#ifdef __linux__
std::string path = "./adventocode8.txt";
#elif _WIN32
std::string path = ".\\adventocode8.txt";
#endif

using namespace std;

//typedef std::vector<std::vector<int>> VV;
//typedef std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> VPP;
//typedef std::vector<VPP> VVPP;
typedef std::pair<std::vector<std::string>, std::vector<std::string>> PV;

std::string fileString;
std::ifstream file;

void printInput(vector<int> &input){
    for(auto &n : input){
        cout << n << ' ';
    }
    cout << endl;
}

void printInput(vector<char> &input){
    for(auto &n : input){
        cout << n << ' ';
    }
    cout << endl;
}

void printInput(PV &input){
    cout << "INPUT:" << endl;
    for(auto &n : input.first){
        cout << n << endl;
    }
    cout << "OUTPUT:" << endl;
    for(auto &n : input.second){
        cout << n << endl;
    }
    cout << endl;
}

//PV getInput(){
//    vector<string> inVec = {};
//    vector<string> outVec = {};
//    file.open((path));
//
//    while(getline(file, fileString)) {
//        if(fileString.find('|') != string::npos){
//            inVec.push_back(fileString);
//        }else{
//            outVec.push_back(fileString);
//        }
//    }
//    file.close();
//
//    return make_pair(inVec, outVec);
//}

PV getInput(){
    vector<string> inVec = {};
    vector<string> outVec = {};
    file.open((path));
    while(getline(file, fileString)) {
//        cout << fileString.find(' ') << endl;
        auto pos = fileString.find('|');
        inVec.push_back(fileString.substr(0, pos -1));
        outVec.push_back(fileString.substr(pos + 2, fileString.length()));
    }
    file.close();

    return make_pair(inVec, outVec);
}

int partOne(PV &input){
    vector<string> elemVec = {};
    for(auto &out : input.second){
        while(out.find(' ') != string::npos){
            elemVec.push_back(out.substr(0, out.find(' ')));
            out.erase(0, out.find(' ') + 1);
        }
        elemVec.push_back(out.substr(0, out.length()));
    }

    vector<int> countVec = {0,0,0,0,0,0,0,0,0,0,0};

    for(auto &digit : elemVec){
        countVec[digit.length()]++;
    }

    printInput(countVec);
    return countVec[2] + countVec[3] + countVec[4] + countVec[7];
}

char findNotInWord(string target, vector<string> source){
    int index;
    for(auto &word: source){
        for(auto &letter: word){
            index = target.find(letter);
            cout << target << ": " << word << " " << letter << " " << endl;
            if(index == string::npos){
                cout << "FOUDNDD" << endl;
                return letter;
            }
        }
    }
    return '\0';
}


string addDigits(string s1, string s2){
    string retString;
    for(auto &letter: s1){
        if(retString.find(letter) == string::npos){
            retString.push_back(letter);
        }
    }
    return retString;
}


string overlayDigits(vector<string> digits){
    auto overlayedDigit = "";
    for(auto &digit:digits){
        for(auto &letter:digit){
            cout << " JOE" ;
        }
    }
    return "";
}

vector<string> getDisplay(vector<string> &input){
    vector<char> returnVec = {'x','x','x','x','x','x','x'};

    map<int, vector<string>> ldict;
    
    for(auto &elem: input){
        auto itr = ldict.find(elem.length());
        if(itr != ldict.end()){
            auto item = ldict[elem.length()];
            item.push_back(elem);
            ldict[elem.length()] = item;
        }else{
            if(elem.length() > 1){
                ldict[int(elem.length())] = {elem};
            }
        }
    }

//    for(auto &e : ldict){
//        auto svec = e.second;
//        cout << e.first << "\t" ;
//        for(auto &s: svec){
//            cout << s << "\t";
//        }
//        cout << endl;
//    }

//    unordered_set<char> allLetters;
//    vector<char> allLetters;
    string allLetters;
    for(auto &digits: ldict){
        for(auto &digit:digits.second){
            for(auto &letter:digit){
                if(allLetters.find(letter) == string::npos){
                    allLetters.push_back(letter);
                }
            }
        }
    }
    for(auto &e:allLetters){
        cout << e;
    }
    cout << endl;
    
    int order[] = {2,7,4,6,3,5};
    for(int orderindex = 0; orderindex != 6; orderindex++){
        if(order[orderindex] == 2){
            auto ret = findNotInWord(ldict[order[orderindex]][0], ldict[3]);
            if(ret != '\0'){
                returnVec[0] = ret;
            }
        }else if(order[orderindex] == 7){
            for(auto &word: ldict[6]){
                for(auto &letter: ldict[order[orderindex]][0]){
                    auto index = word.find(letter);
                    if(index == string::npos){
                        returnVec[3] = letter;
                    }
                }
            }
        }else if(order[orderindex] == 3){
            vector<string> combinedStrings;

            for(auto &word: ldict[6]){
                combinedStrings.push_back(addDigits(word, ldict[order[orderindex]][0]));
            }
            
        }else if(order[orderindex] == 4){
//            returnVec[4] = findNotInWord(elem.second[0], ldict[
            
            std::unordered_set<char> word;
            for(auto &digit: ldict[6]){
                string letterstring;
                for(int i = 0; i < ldict[6].size(); i++){
                    if(digit.find(ldict[6][i]) == string::npos){
                        for(auto &letter : ldict[6][i]){
                            if(letterstring.find(letter) == string::npos && digit.find(letter) == string::npos){
                                letterstring.push_back(letter);
                            }
                        }
                    }
                }
                cout << "L:ETTERSRTIN GG " << letterstring << endl;
                //unordered_set<char> letterset;
                for(auto &e : returnVec){
                    cout << e << " ";
                }
                cout << endl;
//                string letterstring;
//                for(auto &letter: digit){
//                    if(letterstring.find(letter) == string::npos){
//                        letterstring.push_back(letter);
//                    }
//                }
                    
                string tempAllLetters = allLetters;
                for(int i = 0; i < tempAllLetters.size(); i++){
                    cout << letterstring[0];
                    if(find(returnVec.begin(), returnVec.end(), letterstring[0]) == returnVec.end()){

                        returnVec[4] = tempAllLetters[i];
                        cout  << letterstring << " " << returnVec[4] << endl << endl;
                        if(digit.find(ldict[2][0][0]) != string::npos){
                            returnVec[2] = ldict[2][0][0];
                            returnVec[5] = ldict[2][0][1];
                        }else{
                            returnVec[2] = ldict[2][0][1];
                            returnVec[5] = ldict[2][0][0];
                        }
                    }
                }
            }

            for(auto &digit: ldict[5]){
                if(digit.find(returnVec[2]) != string::npos &&
                   digit.find(returnVec[5]) != string::npos){
                    string retVec(returnVec.begin(), returnVec.end());
                    for(char &l: digit){
                        if(retVec.find(l) == string::npos){
                            returnVec[6] = l;
                        }
                    }
                    string retVec2(returnVec.begin(), returnVec.end());
                    for(auto &l: allLetters){
                        if(retVec2.find(l) == string::npos){
                            returnVec[1] = l;
                        }
                    }
                }
            }

            cout << "..." << endl;
        }  
    }

    vector<string> strVec = {"","","","","","",""};
    for(int i =0; i < returnVec.size();i++){
        strVec[i] = returnVec[i];
        cout << i << strVec[i] << " ";
    }
    cout << endl;
    vector<string> digitMap = {"","","","","","","","",""};
    vector<string> digits;
    digits = {"012456", "25", "02356", "02356", "1235", "01356", "013456", "025", "0123456", "012356"};
    for(int i = 0; i < 8; i++){
        for(auto num : digits[i]){
            int intnum = num-48;
            digitMap[i] = digitMap[i].append(strVec[intnum]);
            sort(digitMap[i].begin(), digitMap[i].end());
        }
    }

    return digitMap;
}


int partTwo(PV &input){
//    vector<char> digit = {a,b,c,d,e,f,g};

    vector<vector<string>> inVec = {};

    cout << input.first.size() << " " << input.second.size() << endl;

    for(auto &in : input.first){
        vector<string> tempVec = {};
        //cout << in << endl;
        while(in.find(' ') != string::npos){
            tempVec.push_back(in.substr(0, in.find(' ')));
            in.erase(0, in.find(' ') + 1);
        }
        tempVec.push_back(in.substr(0, in.length()));
        inVec.push_back(tempVec);
    }


    vector<vector<string>> outVec = {};
    for(auto &out : input.second){
     //   cout << out << endl;
        vector<string> tempVec = {};
        while(out.find(' ') != string::npos){
            tempVec.push_back(out.substr(0, out.find(' ')));
            out.erase(0, out.find(' ') + 1);
        }
        tempVec.push_back(out.substr(0, out.length()));
        outVec.push_back(tempVec);
    }

    

    
    int total = 0;
    for(int i = 0; i < input.first.size(); i++){
        string outNum;
        for(auto e: outVec[i]){
            auto display = getDisplay(inVec[i]);
            sort(e.begin(), e.end());
            auto itr = find(display.begin(), display.end(), e);
            if(itr != display.end()){
                int index = itr-display.begin();
                outNum += to_string(index);
            }else{
                cout << e <<  "NOT FOUND!!!" << endl;
            }
        }
        cout << outNum;
        total += stoi(outNum);
    }
    cout << total << endl;
    return 0;
}


int main(){
    auto input = getInput();
    //printInput(input);
    //auto one = partOne(input);
    //cout << "Partone: " << one << endl;
    auto two = partTwo(input);
    cout << "PARTTWO: " << two << endl;
    return 0;
}

//805366914
