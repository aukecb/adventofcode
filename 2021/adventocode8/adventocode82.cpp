#include <iostream>
#include <vector>
#include <fstream>
#include <map>

#ifdef __linux__
std::string path = "./adventocode8.txt";
#elif _WIN32
std::string path = ".\\adventocode8.txt";
#endif

using namespace std;

pair<vector<vector<string>>, vector<vector<string>>> getInput(){
    vector<vector<string>> inVec; 
    vector<vector<string>> outVec;
    ifstream file;
    file.open(path);
    string fileString;

    while(getline(file, fileString)){
        vector<string> elemVec;
        while(fileString.find(' ') != string::npos){
            cout << fileString << fileString.find(' ') << endl;
            if(fileString.find('|') <= 1){
                inVec.push_back(elemVec);
                elemVec.clear(); 
                fileString.erase(0, fileString.find("| ") +2);
            }
            elemVec.push_back(fileString.substr(0, fileString.find(' ') ));
            fileString.erase(0, fileString.find(' ') + 1);
        }
        elemVec.push_back(fileString.substr(0, string::npos));
        outVec.push_back(elemVec);
    }
    file.close();
    return make_pair(inVec, outVec);
}


string getDisplay(auto &input){
    string d = "xxxxxxx";
    map<int, vector<string>> dmap;


    for(auto &line: input){
        for(auto &digit: line){
            cout << digit << digit.size() << endl;
            dmap[digit.length()].push_back(digit);
        }
    }


    return d;
}



int main(){
    auto input = getInput();

    for(int i = 0; i < input.first.size(); i++){
        for(int j = 0; j < input.first[i].size(); j++){
            cout << input.first[i][j] << " " ;
        }
        cout << " || ";
        for(int j = 0; j < input.second[i].size(); j++){
            cout << input.second[i][j] << " ";
        }
        cout << endl;
    }

    cout << input.second[0][input.second[0].size() -1] << endl;

    auto display = getDisplay(input.first);
    cout << display << endl;
    
    return 0;
}

