#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#ifdef __linux__
std::string path = "../adventocode6/adventocode6.txt";
#elif _WIN32
std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\adventocode6\\adventocode6.txt";
#endif

using namespace std;

//typedef std::vector<std::vector<int>> VV;
//typedef std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> VPP;
//typedef std::vector<VPP> VVPP;

std::string fileString;
std::ifstream file;

void printInput(vector<int> &input){
    for(auto &n : input){
        cout << n << ' ';
    }
    cout << endl;
}

void printInput(vector<long long int> &input){
    for(auto &n : input){
        cout << n << ' ';
    }
    cout << endl;
}

vector<int> getInput(){
    vector<int> returnVector = {};
    file.open((path));

    while(getline(file, fileString, ',')) {
        returnVector.push_back(stoi(fileString));
    }
    file.close();

    return returnVector;
}

int partOne(vector<int> &input){
    for(int j =0; j < 80; j++){
        int newFish = 0;
        for(int i = 0; i < input.size(); i++){
            if(input[i] == 0){
                input[i] = 6;
                newFish++;
            }else{
                input[i] -= 1;
            }
        }
        for(int f = 0; f < newFish; f++){
            input.push_back(8);
        }
        cout << j + 1 << ": ";
//        printInput(input);
    }
    return input.size();
}

int partTwo(vector<int> &input){
    vector<long long int> fishVec = {0,0,0,0,0,0,0,0,0};
    for(auto &n : input){
        fishVec[n] +=1;
    }
    long long int totalCount = 0;
//    printInput(fishVec);
    for(int day = 0; day < 256; day++){
        cout << day<< ": ";
        printInput(fishVec);
        long long int addAfterDay = 0;
        for(int i = 0; i < fishVec.size(); i++){
            if(fishVec[i] > 0){
                totalCount += fishVec[i];
                if(i == 0){
                    addAfterDay += fishVec[i];
                    fishVec[7] += fishVec[i];
                    fishVec[0] -= fishVec[i];
                }else{
                    fishVec[i - 1] +=fishVec[i];
                    fishVec[i] -= fishVec[i];
                }
            }
        }
        cout << "TotalCount: " << totalCount<< endl;
        totalCount = 0;
        fishVec[8] = addAfterDay;
    }
    return totalCount;
}


int main(){
    auto input = getInput();
    printInput(input);
    auto one = partOne(input);
    cout << "Partone: " << one << endl;
    auto two = partTwo(input);
    cout << "PARTTWO: " << two << endl;
    return 0;
}

//805366914