#include <fstream>
#include <iostream>
#include <string>
#ifdef __linux__
    std::string path = "../adventocode3/adventocode4.txt";
#elif _WIN32
    std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\adventocode3\\adventocode3.txt";
#endif

#include <vector>




std::string fileString;
std::ifstream file;

std::vector<std::string> getInstructions(){
    std::vector<std::string> returnVector = {};
    std::string direction;
    file.open((path));

    while(std::getline(file, fileString)){
        std::vector<char> row;
//        for(char c : fileString){
//            row.push_back(c);
//        }
        returnVector.push_back(fileString);
    }
    file.close();
    return returnVector;
}

std::vector<int> getMostCommon(std::vector<char> &input){
    int one = 0, zero = 0;
    std::vector<int> oneVec = {1};
    std::vector<int> zeroVec = {0};
    for(int i = 0; i < input.size(); i++){
        if(input[i] == '0'){
            zeroVec.push_back(i);
//            zero++;
        }else if(input[i] == '1'){
            oneVec.push_back(i);
//            one++;
        }
    }

    return (oneVec.size() > zeroVec.size() || oneVec.size() == zeroVec.size()) ? oneVec : zeroVec;
}

std::vector<int> getLeastCommon(std::vector<char> &input){
    std::vector<int> oneVec = {1};
    std::vector<int> zeroVec = {0};
    for(int i = 0; i < input.size(); i++){
        if(input[i] == '0'){
            zeroVec.push_back(i);
//            zero++;
        }else if(input[i] == '1'){
            oneVec.push_back(i);
//            one++;
        }
    }
    return (oneVec.size()  > zeroVec.size() || oneVec.size() == zeroVec.size()) ? zeroVec : oneVec;
}

int binToDec(std::string &input){
    return std::stoi(input, nullptr, 2);
}

int partOne(std::vector<std::string> &input){
    std::string gamma;
    std::string epsilon;

    for(int i = 0; i < input[0].size(); i++){
        std::vector<char> tempvec = {};
        for(auto &j : input){
            tempvec.push_back(j[i]);
        }
        gamma += std::to_string(getMostCommon(tempvec)[0]);
        epsilon += std::to_string(getLeastCommon(tempvec)[0]);
//        std::cout << "Gamma: " << gamma << " Epsilon: " << epsilon <<std::endl;
    }
    return (binToDec(gamma)) * (binToDec(epsilon));
}

int partTwo(std::vector<std::string> &input){
    std::vector<int> oxygenIndex, CO2Index;
    std::vector<std::string> oxygenVec = input, CO2Vec = input;

    auto inputSize = input.size();

    for(int i = 0; i < inputSize; i++){
        std::vector<char> columnVec = {};



        for(auto &c : oxygenVec){
            columnVec.push_back(c[i]);
        }
        oxygenIndex = getMostCommon(columnVec);
        oxygenIndex.erase(oxygenIndex.begin());
        std::vector<std::string> tempVec = {};
        for(auto &index : oxygenIndex){
            tempVec.push_back(oxygenVec[index]);
        }
        oxygenVec = tempVec;

        columnVec = {};
        for(auto &c : CO2Vec){
            columnVec.push_back(c[i]);
        }

        CO2Index = getLeastCommon(columnVec);
        CO2Index.erase(CO2Index.begin());
        tempVec = {};
        for(auto &index : CO2Index) {
            tempVec.push_back(CO2Vec[index]);
        }
        CO2Vec = tempVec;
    }
    return binToDec(oxygenVec[0]) * binToDec(CO2Vec[0]);
}

int main(){
    std::cout << "helloo world\n";
    auto vector = getInstructions();
//    for(auto input : vector){
//        std::cout << input << std::endl;
//    }
    std::cout << "PartOne: " <<  partOne(vector) << std::endl;
    std::cout << "PartTwo: " << partTwo(vector) << std::endl;
	return 0;
}