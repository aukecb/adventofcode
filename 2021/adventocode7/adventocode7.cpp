#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#ifdef __linux__
std::string path = "../adventocode7/adventocode7.txt";
#elif _WIN32
std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\adventocode4\\adventocode4.txt";
#endif

typedef std::vector<int> VI;

std::string fileString;
std::ifstream file;

VI getInstructions(){
    std::vector<int> returnVector = {};
    file.open((path));
    while(getline(file, fileString, ',')){
//        std::cout << fileString << std::endl;
        returnVector.push_back(std::stoi(fileString));
    }
    file.close();
    return returnVector;
}

int partOne(std::vector<int> &input){
    int max = *std::max_element(input.begin(), input.end());
    int min = *std::min_element(input.begin(), input.end());
    VI totalFuelUsed = {};
    for(int i = min; i < max; i++){
        auto tempFuel = 0;
        for(auto &pos : input){
            tempFuel += abs(pos - i);
        }
        totalFuelUsed.push_back(tempFuel);
    }

    return *std::min_element(totalFuelUsed.begin(), totalFuelUsed.end());
}

int partTwo(VI &input){
    int max = *std::max_element(input.begin(), input.end());
    int min = *std::min_element(input.begin(), input.end());
//    std::cout << "MIN: " << min << " MAX: " << max <<std::endl;
    VI totalFuelUsed = {};
    for(int i = min; i < max; i++){
//        std::cout << "CHECKING: " << i << std::endl;
        auto tempFuel = 0;
        for(auto &pos : input){
            tempFuel +=  (abs(pos - i) * (abs(pos -i) +1)) /2;
        }
//        std::cout << tempFuel << std::endl;
        totalFuelUsed.push_back(tempFuel);
    }

    return *std::min_element(totalFuelUsed.begin(), totalFuelUsed.end());
}

int main(){
    auto input = getInstructions();
    auto answer1 = partOne(input);
    std::cout << "PartOne: " << answer1 << std::endl;
    auto answer2 = partTwo(input);
    std::cout << "PartTwo: " << answer2 << std::endl;
return 0;
}