#include <fstream>
#include <iostream>
#include <string>
#include <vector>

std::string path = "D:\\Users\\aukem\\Documents\\_git\\private\\adventofcode\\";
std::string fileString;
std::ifstream file;

std::vector<std::pair<std::string, int>> getInstructions(){
    std::vector<std::pair<std::string, int>> returnVector = {};
    int steps;
    std::string direction;
    int horizontal = 0, depth = 0;
    file.open((path + "adventocode2\\adventocode4.txt"));
    while(getline(file, fileString)){
        if(fileString.find(' ')){
            steps = std::stoi(fileString.substr(fileString.find(' ') + 1));
            direction = fileString.substr(0, fileString.find(' '));
            returnVector.push_back(std::make_pair(direction, steps));
        }
    }
    file.close();
    return returnVector;
}

std::pair<int, int> partOne(std::vector<std::pair<std::string, int>> direction){
    int horizontal = 0, depth = 0;
    for(int i = 0; i < direction.size(); i++){
        if(direction[i].first == "forward"){
            horizontal += direction[i].second;
        }else if(direction[i].first == "up"){
            depth -= direction[i].second;
        }else if(direction[i].first == "down"){
            depth += direction[i].second;
        }
    }
    return std::make_pair(horizontal, depth);
}

std::pair<int, int> partTwo(std::vector<std::pair<std::string, int>> direction){
    int horizontal = 0, depth = 0, aim = 0;
    for(int i=0; i < direction.size(); i++){
        if(direction[i].first == "forward"){
            horizontal += direction[i].second;
            depth += direction[i].second * aim;
        }else if(direction[i].first == "up"){
            aim -= direction[i].second;
        }else if(direction[i].first == "down"){
            aim += direction[i].second;
        }
        std::cout << "horizontal " << horizontal << "  depth  " << depth <<  "  aim  " << aim << std::endl;
    }

    return std::make_pair(horizontal, depth);
}

int main(){
    auto answer1 = partOne(getInstructions());
    auto answer2 = partTwo(getInstructions());

    std::cout << "horizontal " << answer1.first << std::endl;
    std::cout << "depth " << answer1.second << std::endl;
    std::cout << "multiply " << answer1.first * answer1.second << std::endl;

    std::cout << "horizontal " << answer2.first << std::endl;
    std::cout << "depth " << answer2.second << std::endl;
    std::cout << "multiply " << answer2.second * answer1.first << std::endl;
	return 0;
}